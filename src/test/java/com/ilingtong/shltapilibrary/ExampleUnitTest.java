package com.ilingtong.shltapilibrary;

import android.widget.Toast;

import com.ilingtong.shltapilibrary.bean.UpdateVersionBodyBean;
import com.ilingtong.shltapilibrary.manager.AppActionManager;
import com.ilingtong.shltapilibrary.manager.listener.AbstractActionCallbackListener;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}