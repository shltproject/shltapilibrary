package com.ilingtong.shltapilibrary.utils;

import android.util.Log;

import com.ilingtong.shltapilibrary.ApiLibApplication;
import com.ilingtong.shltapilibrary.BuildConfig;
import com.ilingtong.shltapilibrary.R;

/**
 * Created by wuqian on 2016/9/12.
 * mail: wuqian@ilingtong.com
 * Description:普通信息打印
 */
public class LogUtils {
    public static final boolean DEBUG = BuildConfig.DEBUG;
    public static final String TAG = ApiLibApplication.mInstance.getString(R.string.config_logcat_tag);
    private LogUtils() {

    }

    public static void d(String msg) {
        if (DEBUG) {
            d(TAG, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (DEBUG) {
            Log.d(tag, msg);
        }
    }

    public static void e(String msg) {
        if (DEBUG) {
            e(TAG, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (DEBUG) {
            Log.e(tag, msg);
        }
    }

    public static void v(String msg) {
        if (DEBUG) {
            v(TAG, msg);
        }
    }

    public static void v(String tag, String msg) {
        if (DEBUG) {
            Log.v(tag, msg);
        }
    }

    public static void exception(String msg) {
        e(msg);
    }
}
