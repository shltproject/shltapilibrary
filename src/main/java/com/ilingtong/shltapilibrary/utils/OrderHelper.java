package com.ilingtong.shltapilibrary.utils;


import com.ilingtong.shltapilibrary.ApiLibApplication;
import com.ilingtong.shltapilibrary.R;

/**
 * Package:com.ilingtong.shltapilibrary.utils
 * author: liuting
 * Date: 2016/9/21
 * Email:liuting@ilingtong.com
 * Desc:订单辅助类
 */
public class OrderHelper {
    //订单状态
    public static final String ORDER_NEW = "0";             //	新单
    public static final String ORDER_REVIEWED = "1";        //	已审核
    public static final String ORDER_PAID = "2";            //	已付款
    public static final String ORDER_SENDING = "3";         //	发货中
    public static final String ORDER_NO_COMMENTS = "4";    //	完成未评价
    public static final String ORDER_HAVE_COMMENTS = "5";   //	已评价
    public static final String ORDER_REJECT = "6";          //	拒收
    public static final String ORDER_NO_PAY = "7";          //	待付款
    public static final String ORDER_PAY_CONFIRM = "8";     //付款确认中(微信支付使用)
    public static final String ORDER_CANCEL = "12";

    public static String getStatus(String status){
       switch (status){
           case ORDER_NEW://新单
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.in_review);
           case ORDER_REVIEWED://已审核
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.reviewed);
           case ORDER_PAID://已付款，待发货
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.not_receive);
           case ORDER_SENDING://发货中
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.shipping);
           case ORDER_NO_COMMENTS://完成未评价
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.not_evaluate);
           case ORDER_HAVE_COMMENTS://已评价
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.evaluated);
           case ORDER_REJECT://拒收
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.rejected);
           case ORDER_NO_PAY://待付款
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.not_pay);
           case ORDER_PAY_CONFIRM://付款确认中
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.payment_is_in_confirmation);
           case ORDER_CANCEL:  //订单取消
               return ApiLibApplication.mInstance.getApplicationContext().getString(R.string.order_cancel);
           default:
               return "";
       }
    }
}
