package com.ilingtong.shltapilibrary.utils;

/**
 * Package:com.ilingtong.shltapilibrary.utils
 * author:liuting
 * Date:2016/11/1
 * Desc:格式校验类
 */

public class CheckUtils {

    /**
     * 校验邮箱格式是否正确
     *
     * @param email  邮箱
     * @return
     */
    public static boolean IsEmailBox(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
