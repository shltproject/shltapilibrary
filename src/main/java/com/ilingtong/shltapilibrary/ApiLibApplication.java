package com.ilingtong.shltapilibrary;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ilingtong.shltapilibrary.bean.ConfigInfoBean;
import com.ilingtong.shltapilibrary.constant.Constants;

import java.util.Locale;

/**
 * Created by wuqian on 2016/8/9.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public abstract class ApiLibApplication extends Application {
    public static ApiLibApplication mInstance;
    public ConfigInfoBean mConfigInfoBean;
    private SharedPreferences mConfigSp;

    @Override
    public void onCreate() {
        super.onCreate();
        mConfigSp = getSharedPreferences("configInfo", Context.MODE_PRIVATE);
        mConfigInfoBean = new ConfigInfoBean(mConfigSp);
        initConfig();
        mInstance = this;
    }

    /**
     * 初始化configInfo。
     * 必须设置包名，版本号等信息
     */
    public abstract void initConfig();

    public ConfigInfoBean getmConfigInfoBean() {
        if (mConfigInfoBean == null) {
            mConfigInfoBean = new ConfigInfoBean(mConfigSp);
        }
        return mConfigInfoBean;
    }

    /**
     * 获取当前系统语言
     *
     * @return
     */
    public String getLanguage() {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.LOGIN_SP_KEY_NAME, MODE_PRIVATE);
        String language = sharedPreferences.getString(Constants.LANGUAGE, Constants.LANGUAGE_EN);
        if (language.contains("zh")) {
            language = language.contains("TW") || language.contains("HK") ? Constants.LANGUAGE_TW : Constants.LANGUAGE_CN;
        } else {
            language = Constants.LANGUAGE_EN;
        }
        Log.e("tag", "language:" + language);
        return language;
    }
}
