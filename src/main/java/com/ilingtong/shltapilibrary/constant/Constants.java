package com.ilingtong.shltapilibrary.constant;

/**
 * Created by wuqian on 2016/8/3.
 * mail: wuqian@ilingtong.com
 * Description:常量定义class
 */
public class Constants {
    /**
     * Http connect time out time
     */
    public static final int CONNECT_TIMEOUT_MS = 40000;

    public static final String SUCCESS = "1";
    public static final String FETCH_COUNT = "10";     //列表接口每次请求数据条数
    //翻页
    public static final String FORWORD_DONW = "1";  //向下翻页，加载更多


    /**********
     * 接口文档中定义的常量
     ****************/

    //是否。也表示接口调用成功或失败
    public static final String YES = "0";  // 是/成功
    public static final String NO = "1";
    public static final String TOKEN_FAIL = "2";  //接口返回错误类型 。return_flag为该值时表示token失效

    public static final String PHONE = "3";  //移动平台编号 -- 手机。用户版本更新接口，区分phone和pad

    //订单状态过滤条件
    public static final String ORDER_FILTER_TOTAL = "0";    //	全部
    public static final String ORDER_FILTER_PENDING_REVIEW = "1";   //	待审核
    public static final String ORDER_FILTER_PENDING_PAY = "2";  //	待付款
    public static final String ORDER_FILTER_PENGDING_DELIVERY = "3";    //	待收货
    public static final String ORDER_FILTER_PENDING_COMMENT = "4";  //	待评价

    //商品明细入口
    public static final String PROD_ACTION_HOME = "1";     //	首页活动
    public static final String PROD_ACTION_POST = "2";     //	帖子链接
    public static final String PROD_ACTION_SCAN = "4";     //	扫描条码
    public static final String PROD_ACTION_CART = "5";     //	购物车
    public static final String PROD_ACTION_MSTOR = "6";     //	魔店

    //评价等级	1046接口入口参数 level取值
    public static final String PROD_EVAL_LEVEL_ALL = "0";     //全部
    public static final String PROD_EVAL_LEVEL_NICE = "1";     //好评
    public static final String PROD_EVAL_LEVEL_MEDIUM = "2"; //中评
    public static final String PROD_EVAL_LEVEL_LOW = "3";     //差评

    //收藏类别
    public static final String FAVORITE_TYPE_PROD = "0";   //	商品
    public static final String FAVORITE_TYPE_MSTORE = "1";   //	魔店
    public static final String FAVORITE_TYPE_EXPERT = "2";   //	会员
    public static final String FAVORITE_TYPE_POST = "3";   //	帖子
    public static final String FAVORITE_TYPE_BABYPROD = "4";   //	宝贝商品

    //用户区分
    public static final String USER_TYPE_CONSUMER = "1";  //	消费者
    public static final String USER_TYPE_MEMBER = "2";  //	会员
    public static final String USER_TYPE_FREEMEN = "3";  //	游客

    //支付方式：支付宝、网银、微信
    public static final String PAY_TYPE_ALIPAY = "1";  //支付宝
    public static final String PAY_TYPE_WECHAT = "3";  //微信
    public static final String PAY_TYPE_BANK = "4";  //网银
    public static final String PAY_TYPE_UNIONPAY = "5";  //中银支付

    //语言常量定义
    public static final String LANGUAGE_CN = "zh-CN";  //中文
    public static final String LANGUAGE_EN = "en-US";  //英文
    public static final String LANGUAGE_TW = "zh-TW";  //繁体中文

    //语言SP存储key
    public static final String LOGIN_SP_KEY_NAME = "login";//登录信息缓存标识
    public static final String LANGUAGE = "language";   //语言

}
