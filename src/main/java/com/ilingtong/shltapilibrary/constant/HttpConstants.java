package com.ilingtong.shltapilibrary.constant;

import com.ilingtong.shltapilibrary.ApiLibApplication;

/**
 * Created by wuqian on 2016/8/8.
 * mail: wuqian@ilingtong.com
 * Description:用来定义接口相关的常量
 */
public class HttpConstants {
    public static String HTTP_ADDRESS = "http://www.tongler.cn/taxapi/1.0/";

    //测试版
    //public static String HTTP_ADDRESS = "http://www.tongler.cn/taxapi/2.0/";

    public static String SYSTEM_URL = HTTP_ADDRESS + "system/";
    public static String TOKEN_URL = HTTP_ADDRESS + ApiLibApplication.mInstance.mConfigInfoBean.getToken() + "/";
    public static String CEDRUS_LOGIN_URL = SYSTEM_URL + "ts/user_login";  //退税版登录 11001
    public static String UPDATEVERSION_URL = SYSTEM_URL + "version_check";
    public static String CEDRUS_RESISTER_URL = SYSTEM_URL + "ts/register_info_submit";  //退税版注册 11001
    public static String CEDRUS_FORGET_PDW = SYSTEM_URL + "ts/password_reset";   //退税版忘记密码 11003
    public static String TEMP_LOGIN_URL = SYSTEM_URL + "Templogin";   //游客登录 2044
    public static String GET_SHOPCART_LIST_URL = TOKEN_URL + "shopping_carts/show";   //获取我的购物车列表 1026
    public static String CREAT_ORDER_URL = TOKEN_URL + "orders/create";   //新增订单 1029
    public static String GET_ADDRESS_LIST_URL = TOKEN_URL + "my/addresses";   //获取我的地址列表 1030
    public static String GET_BASE_DATA_URL = SYSTEM_URL + "base_data";    //获取基础数据 2018
    public static String GET_ORDER_LIST_URL = TOKEN_URL + "orders/user_orders";                      //我的订单列表全部	1040
    public static String GET_ORDER_DETAIL_URL = TOKEN_URL + "orders/show";                     //订单详情	1041
    public static String GET_ORDER_APPRAISAL_URL = TOKEN_URL + "products/evaluation_show";                      // 获取商品评价 1046
    public static String ADD_TO_SHOPCART_URL = TOKEN_URL + "shopping_carts/create";                     //加入购物车
    public static String GET_PROD_DETAIL_URL = TOKEN_URL + "products/show";                 //获取商品详情 1022
    public static String ADD_FAVORITE_URL = TOKEN_URL + "favorites/add";                      //收藏 2021
    public static String CANCEL_FAVORITE_URL = TOKEN_URL + "favorites/remove";                      // 取消收藏       1088
    public static String GET_PROD_TXET_PIC_URL = TOKEN_URL + "products/image_text_show";                      //获取商品图文详情	1024
    public static String GET_PROD_SPEC_URL = TOKEN_URL + "products/specification_show";                      //获取商品规格参数	1023
    public static String GTE_FAROVITE_PROD_LIST_URL = TOKEN_URL + "favorites/user_products";                      //我收藏的商品列表	2019
    public static String GET_POST_DETAIL_URL = TOKEN_URL + "posts/post_show";                      //帖子详情 （无收藏）	1089
    public static String DECODE_QR_URL = TOKEN_URL + "tools/scan_decode";                      //解析二维码	1064
    public static String ADD_ADDRESS_URL = TOKEN_URL + "my/address_create";                      //添加收货地址	1031
    public static String UPDATE_ADDRESS_URL = TOKEN_URL + "my/address_modify";                //修改收货地址 1032
    public static String GET_APP_QRCODE_URL = TOKEN_URL + "system/app_promote";                      //app二维码推广	2015
    public static String GET_USER_QRCODE_URL = TOKEN_URL + "qrcode/user_show";                     //去个人二维码	1043
    public static String REMOVE_CART_PROD = TOKEN_URL + "shopping_carts/item_remove";   //删除购物车中商品 1027
    public static String UPDATE_CART_PROD_NUM_URL = TOKEN_URL + "shopping_carts/item_modify";     //修改购物车商品数量  1028
    public static String GET_INTRODUCTION_URL = SYSTEM_URL + "function_introduction";     //获取功能介绍页（h5）的url地址和标题
    public static String GET_USER_AGREEMENT_URL = SYSTEM_URL + "user_agreement";  //用户协议及版权申明URL取得 2025
    public static String REMOVE_ADDRESS_URL = TOKEN_URL + "my/address_remove";  //删除收货地址 1033
    public static String SET_DEFAULT_ADDRESS_URL = TOKEN_URL + "my/address_default";   //设置默认收货地址 2023
    public static String DELETE_ORDER_BY_NO_URL = TOKEN_URL + "orders/remove";   //删除订单 1042
    public static String ORDER_COMFIRM_RECEIPT_URL = TOKEN_URL + "orders/confirm";   //确认收货 2024
    public static String EVALUATE_PROD_URL = TOKEN_URL + "products/evaluation_create";  //评价商品  1045
    public static String GET_PROD_QR_URL = TOKEN_URL + "qrcode/product_show";    //动态获取商品二维码 1085
    public static String SETTLE_CART_PROD_URL = TOKEN_URL + "shopping_carts/balance";   //购物车商品结算 2042
    public static String SETTLE_PROD_URL = TOKEN_URL + "care_shopping_carts/balance";    //商品结算（立即购买，不走购物车流程） 12002
    public static String CREATE_ORDER_NOW = TOKEN_URL + "care_orders/create";    //新增订单（立即购买，不走购物车流程）12003
    public static String GET_PICK_UP_ADDRESS_LIST_URL = TOKEN_URL + "ts/since_some_show";  //获取自提点列表
    public static String GET_USER_ENTRY_INFO_URL = TOKEN_URL + "ts/immigration_info";   //获取用户护照，入境等信息
    public static String UPLOAD_ENTRY_INFO_URL = TOKEN_URL + "ts/user_info_submit";   //上传护照，入境信息
    public static String GET_WXPAY_PARAMETER_URL = TOKEN_URL + "orders/weixin_pay_parameter";   //获取微信支付参数
    public static String GET_ALIPAY_PARAMETER_URL = TOKEN_URL + "orders/alipay_parameter";   //获取支付宝支付参数
    public static String GET_PERSONAL_INFO_URL = TOKEN_URL + "my/personal_info_show";      //获取我的个人信息
    public static String GET_SHOPCART_WITH_STORE = TOKEN_URL + "shopping_carts/show_new";   //退税版获取我的购物车
    public static String CANCEL_PAID_ORDER = TOKEN_URL + "orders/tax_cancel";    //取消已付款订单
    public static String PRODUCT_SHARE_URL = TOKEN_URL + "products/repost";//商品分享
    public static String POST_SHARE_URL = TOKEN_URL + "posts/repost";//帖子分享
    public static String USEFUL_COUPON_URL = TOKEN_URL + "orders/useful_vouchers_info";//获取可用优惠券url
    public static String GET_UNIONPAY_PARAM_URL = TOKEN_URL + "zy/pay_order";  //中银支付，取的支付的URL
    public static String UPDATE_ACCOUNT_INFO_URL = TOKEN_URL + "my/personal_info_modify";  //修改用户信息的URL
    public static String DECODE_LISTEN_DATA_URL = TOKEN_URL + "tools/voice_decode";  //音频数据解析
    public static String HELP_SETTING_URL = HTTP_ADDRESS + "system/contact";  //设置-帮助
    public static String GET_GUIDE_INFO_URL = TOKEN_URL + "guide/guide_info";  //获取导游信息
    public static String BIND_GUIDE__URL = TOKEN_URL + "guide/guide_bind";  //绑定获取导游
    public static String UNBIND_GUIDE__URL = TOKEN_URL + "guide/guide_relieve";  //解绑导游
    public static String DECODE_GUIDE_URL = TOKEN_URL + "tools/scan_user_decode";  //解码导游的扫描的结果
}
