package com.ilingtong.shltapilibrary.manager.listener;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.ilingtong.shltapilibrary.ApiLibApplication;
import com.ilingtong.shltapilibrary.constant.ErrorEvent;
import com.ilingtong.shltapilibrary.utils.ToastUtils;

/**
 * Created by wuqian on 2016/8/4.
 * mail: wuqian@ilingtong.com
 * Description:回调监听器ActionCallbackListener，回调监听器的泛型则是返回的对象数据类型
 */
public abstract class AbstractActionCallbackListener<T> {
    private Context context;

    public AbstractActionCallbackListener(Context context) {
        this.context = context;
    }

    /**
     * 成功时调用
     *
     * @param data 返回的数据
     */
    public abstract void onSuccess(T data);

    /**
     * 失败时调用
     *
     * @param errorEvent 错误码
     * @param message    错误信息
     */
    public void onFailure(String errorEvent, String message) {

        if (ErrorEvent.NETWORK_TOKEN_FAIL_EVENT.equals(errorEvent)) {  //token失效，跳转到登录页面
            Intent intent = new Intent(ApiLibApplication.mInstance.mConfigInfoBean.getLoginActity());
            context.startActivity(intent);
            return;
        }else if (ErrorEvent.NETWORK_OTHER_ERROR_EVENT.equals(errorEvent)){  //接口return flag返回错误，显示message
            ToastUtils.showLongToast(context,message);
            return;
        }else if (ErrorEvent.NETWORK_VOLLEY_ERROR_EVENT.equals(errorEvent)){
            ToastUtils.showShortToast(context,message);
        }else if (ErrorEvent.PARAM_ILLEGAL.equals(errorEvent)){
            ToastUtils.showLongToast(context,message);
        }else if (ErrorEvent.PARAM_NULL.equals(errorEvent)){
            ToastUtils.showLongToast(context,message);
        }
    }
}
