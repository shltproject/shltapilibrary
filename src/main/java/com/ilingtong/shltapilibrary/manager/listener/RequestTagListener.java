package com.ilingtong.shltapilibrary.manager.listener;

/**
 * Created by wuqian on 2016/8/10.
 * mail: wuqian@ilingtong.com
 * Description: 用于request请求时添加tag
 */
public interface RequestTagListener {
    void  addRequestTag(String tag);
}
