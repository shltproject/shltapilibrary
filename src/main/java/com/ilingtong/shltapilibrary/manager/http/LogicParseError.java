package com.ilingtong.shltapilibrary.manager.http;

import com.android.volley.VolleyError;

/**
 * Created by wuqian on 2016/8/5.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class LogicParseError extends VolleyError{
    public String code;
    public String message;
    public LogicParseError(String code,String message){
        this.code = code;
        this.message = message;
    }
}
