package com.ilingtong.shltapilibrary.manager;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.ilingtong.shltapilibrary.ApiLibApplication;
import com.ilingtong.shltapilibrary.R;
import com.ilingtong.shltapilibrary.bean.AddaddressRequestBean;
import com.ilingtong.shltapilibrary.bean.AddressListBean;
import com.ilingtong.shltapilibrary.bean.AddressNoBean;
import com.ilingtong.shltapilibrary.bean.AgreementBean;
import com.ilingtong.shltapilibrary.bean.AlipayParameterBean;
import com.ilingtong.shltapilibrary.bean.BaseDataInfoBean;
import com.ilingtong.shltapilibrary.bean.BaseDataListBean;
import com.ilingtong.shltapilibrary.bean.BaseDataTypeNameBean;
import com.ilingtong.shltapilibrary.bean.BaseDataTypesBean;
import com.ilingtong.shltapilibrary.bean.FavoritesInfoRequestBean;
import com.ilingtong.shltapilibrary.bean.GuideInfoBean;
import com.ilingtong.shltapilibrary.bean.HelpInfoBean;
import com.ilingtong.shltapilibrary.bean.IntroducitonBean;
import com.ilingtong.shltapilibrary.bean.LoginUserInfoBean;
import com.ilingtong.shltapilibrary.bean.OrderCreateNowOrderRequestBean;
import com.ilingtong.shltapilibrary.bean.OrderCreateOrderInfoRequestBean;
import com.ilingtong.shltapilibrary.bean.OrderCreateResultBean;
import com.ilingtong.shltapilibrary.bean.OrderDeleteBean;
import com.ilingtong.shltapilibrary.bean.OrderDetailOrderBean;
import com.ilingtong.shltapilibrary.bean.OrderListBean;
import com.ilingtong.shltapilibrary.bean.PersonalInfoBean;
import com.ilingtong.shltapilibrary.bean.PickUpAddressBean;
import com.ilingtong.shltapilibrary.bean.PostShareBean;
import com.ilingtong.shltapilibrary.bean.PostUserPostBean;
import com.ilingtong.shltapilibrary.bean.ProdBaseInfoBean;
import com.ilingtong.shltapilibrary.bean.ProdCartItemRequestBean;
import com.ilingtong.shltapilibrary.bean.ProdEvaluationListBean;
import com.ilingtong.shltapilibrary.bean.ProdListBean;
import com.ilingtong.shltapilibrary.bean.ProdParametersListBean;
import com.ilingtong.shltapilibrary.bean.ProdPicAndTextListBean;
import com.ilingtong.shltapilibrary.bean.ProdQrInfoBean;
import com.ilingtong.shltapilibrary.bean.ProductShareBean;
import com.ilingtong.shltapilibrary.bean.RequestParamHeadBean;
import com.ilingtong.shltapilibrary.bean.ScanQrDataBean;
import com.ilingtong.shltapilibrary.bean.SettleProdRequestBean;
import com.ilingtong.shltapilibrary.bean.SettlementInfoBean;
import com.ilingtong.shltapilibrary.bean.ShopCartBean;
import com.ilingtong.shltapilibrary.bean.ShopCartListBean;
import com.ilingtong.shltapilibrary.bean.ShopCartProdRequstBean;
import com.ilingtong.shltapilibrary.bean.UnionPayParamsBean;
import com.ilingtong.shltapilibrary.bean.UpdateAccountInfoBean;
import com.ilingtong.shltapilibrary.bean.UpdateAccountInfoRequestBean;
import com.ilingtong.shltapilibrary.bean.UpdateVersionBodyBean;
import com.ilingtong.shltapilibrary.bean.UrlAppDownLinkBean;
import com.ilingtong.shltapilibrary.bean.UsefulCouponBean;
import com.ilingtong.shltapilibrary.bean.UsefulCouponRequestBean;
import com.ilingtong.shltapilibrary.bean.UserEntryBean;
import com.ilingtong.shltapilibrary.bean.UserEntryRequestBean;
import com.ilingtong.shltapilibrary.bean.WXpayParameterBean;
import com.ilingtong.shltapilibrary.constant.Constants;
import com.ilingtong.shltapilibrary.constant.ErrorEvent;
import com.ilingtong.shltapilibrary.constant.HttpConstants;
import com.ilingtong.shltapilibrary.manager.http.RequestHelper;
import com.ilingtong.shltapilibrary.manager.listener.AbstractActionCallbackListener;
import com.ilingtong.shltapilibrary.manager.listener.BaseDataCallBackListener;
import com.ilingtong.shltapilibrary.manager.listener.RequestTagListener;
import com.ilingtong.shltapilibrary.utils.CheckUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;

/**
 * Created by wuqian on 2016/8/8.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class AppActionManager {
    /**
     * 格式化入口参数，添加head信息
     *
     * @param requestParam 未添加head前的原始参数map
     * @param isSystem     是否是系统接口。为true时表示不需要设置userid和token。
     * @return
     */
    public static Map<String, String> getParams_gson(Map requestParam, boolean isSystem) {
        Map<String, String> params_gson = new HashMap();
        Gson mGson = new Gson();
        RequestParamHeadBean head;
        if (isSystem) {
            head = new RequestParamHeadBean("", "");
        } else {
            head = new RequestParamHeadBean(ApiLibApplication.mInstance.getmConfigInfoBean().getUserid(), ApiLibApplication.mInstance.getmConfigInfoBean().getToken());
        }
        requestParam.put("head", mGson.toJson(head));
        params_gson.put("parameters_json", mGson.toJson(requestParam).toString());
        return params_gson;
    }

    /**
     * 为需要userid和token的接口格式化入口参数，添加head信息。
     *
     * @param requestParam 未添加head前的原始参数map
     * @return
     */
    public static Map<String, String> getParams_gson(Map requestParam) {
        return getParams_gson(requestParam, false);
    }

    /**
     * 退税版登录 11002
     *
     * @param mailbox
     * @param password
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void doLoginForCedrus(Context context, String mailbox, String password, boolean isShowDialog, AbstractActionCallbackListener<LoginUserInfoBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(mailbox)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.mailbox_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(password)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.password_can_not_null));
            return;
        }
        if (!CheckUtils.IsEmailBox(mailbox)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_ILLEGAL, context.getString(R.string.mailbox_can_not_match));
            return;
        }

        Map<String, String> params = new HashMap<>();
        params.put("mailbox", mailbox);
        params.put("password", password);
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.CEDRUS_LOGIN_URL, params, LoginUserInfoBean.class, abstractActionCallbackListener, tagListener, isShowDialog);
    }

    /**
     * 退税版注册 11001
     *
     * @param context
     * @param guideCode                                         导游码 （不能为空）
     * @param mailbox                                           邮箱 （不能为空）
     * @param password                                          密码 （不能为空)
     * @param phone                                             电话号码
     * @param abstractActionCallbackListener<LoginUserInfoBean> 注意这里注册成功后只返回userid。LoginUserInfoBean的其他变量值都为空
     * @param tagListener
     */
    public static void doRegisterForCedrus(Context context, String guideCode, String mailbox, String password, String phone, AbstractActionCallbackListener<LoginUserInfoBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(guideCode)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.guide_code_null));
            return;
        }
        if (TextUtils.isEmpty(mailbox)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.mailbox_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(password)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.password_can_not_null));
            return;
        }
        if (!CheckUtils.IsEmailBox(mailbox)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_ILLEGAL, context.getString(R.string.mailbox_can_not_match));
            return;
        }

        Map<String, String> params = new HashMap<>();
        params.put("guide_id", guideCode);
        params.put("mailbox", mailbox);
        params.put("password", password);
        params.put("phone", phone);
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.CEDRUS_RESISTER_URL, params, LoginUserInfoBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 11003 退税版忘记密码
     *
     * @param context
     * @param mailbox                        邮箱
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void sendPasswordToMailboxForCedrus(Context context, String mailbox, AbstractActionCallbackListener abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(mailbox)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.mailbox_can_not_null));
            return;
        }
        if (!CheckUtils.IsEmailBox(mailbox)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_ILLEGAL, context.getString(R.string.mailbox_can_not_match));
            return;
        }

        Map<String, String> params = new HashMap<>();
        params.put("mailbox", mailbox);
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.CEDRUS_FORGET_PDW, params, null, abstractActionCallbackListener, tagListener);
    }

    /**
     * 2044 游客登录
     *
     * @param context
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void templogin(Context context, AbstractActionCallbackListener<LoginUserInfoBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.TEMP_LOGIN_URL, params, LoginUserInfoBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1026 取得我的购物车列表
     *
     * @param context
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getShopCart(Context context, AbstractActionCallbackListener<ShopCartListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.GET_SHOPCART_LIST_URL, params, ShopCartListBean.class, abstractActionCallbackListener, tagListener, false);
    }

    /**
     * 1028 修改购物车商品数量
     *
     * @param context
     * @param seq_no                         明细序号
     * @param product_id                     商品编号
     * @param order_qty                      数量
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void updateCartProdNum(Context context, String seq_no, String product_id, String order_qty, AbstractActionCallbackListener abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(seq_no)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.serial_number_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(product_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prodid_can_not_null));
            return;
        }
        if (Integer.parseInt(order_qty) < 1) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_ILLEGAL, context.getString(R.string.num_less_than_one));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("seq_no", seq_no);
        params.put("product_id", product_id);
        params.put("order_qty", order_qty);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.UPDATE_CART_PROD_NUM_URL, params, null, abstractActionCallbackListener, tagListener, false);
    }

    /**
     * 1027 删除购物车中商品
     *
     * @param context
     * @param list                           需要删除的商品的集合。可同时删除多条商品
     *                                       <ShopCartProdRequstBean>
     *                                       public String seq_no; //明细序号
     *                                       public String product_id; //商品ID
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void removeCartProds(Context context, ArrayList<ShopCartProdRequstBean> list, AbstractActionCallbackListener abstractActionCallbackListener, RequestTagListener tagListener) {
        if (list.size() == 0) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.selected_prod_null));
            return;
        }
        Map<String, String> params = new HashMap<String, String>();

        Map<String, ArrayList> param = new HashMap<>();
        param.put("detail", list);
        Gson mGson = new Gson();
        String delGson = mGson.toJson(param).toString();
        params.put("detail_json", delGson);

        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.REMOVE_CART_PROD, params, null, abstractActionCallbackListener, tagListener);
    }

    /***
     * 1001 版本更新
     *
     * @param context
     * @param local_app_version              app本地版本号
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void doUpdateVersion(Context context, String local_app_version, boolean isShowDialog, AbstractActionCallbackListener<UpdateVersionBodyBean> abstractActionCallbackListener, RequestTagListener tagListener) {

        if (TextUtils.isEmpty(local_app_version)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.local_app_version_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("mobile_os_no", Constants.PHONE);
        params.put("local_app_version", local_app_version);
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.UPDATEVERSION_URL, params, UpdateVersionBodyBean.class, abstractActionCallbackListener, tagListener, isShowDialog);
    }

    /**
     * 1029 新增订单 （适用于已加入购物车的商品提交订单）
     *
     * @param context
     * @param orderInfo
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void createOrder(Context context, OrderCreateOrderInfoRequestBean orderInfo, AbstractActionCallbackListener<OrderCreateResultBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(orderInfo.shipping_method.shipping_method_no)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_shipping_method));
            return;
        }
        if (TextUtils.isEmpty(orderInfo.since_some_id) && TextUtils.isEmpty(orderInfo.address_no)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_address));
            return;
        }
        if (TextUtils.isEmpty(orderInfo.receive_date) && TextUtils.isEmpty(orderInfo.shipping_method.shipping_time_memo)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_receive_date));
            return;
        }
        if (orderInfo.product_list.size() == 0) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prod_info_can_not_null));
            return;
        }

        Map<String, OrderCreateOrderInfoRequestBean> orderInfoParams = new HashMap<String, OrderCreateOrderInfoRequestBean>();
        orderInfoParams.put("order_info", orderInfo);
        Gson mGson = new Gson();

        Map<String, String> params = new HashMap<>();
        params.put("order_info_json", mGson.toJson(orderInfoParams));
        params.put("buy_user_id", ApiLibApplication.mInstance.mConfigInfoBean.getUserid());
        params.put("pay_scene", "");
        params.put("sign_pic", "");

        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.CREAT_ORDER_URL, params, OrderCreateResultBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1030 获取我的地址列表
     *
     * @param context
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getAddressList(Context context, AbstractActionCallbackListener<AddressListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<String, String>();
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.GET_ADDRESS_LIST_URL, params, AddressListBean.class, abstractActionCallbackListener, tagListener, false);
    }

    /**
     * 1033 删除收货地址
     *
     * @param context
     * @param addressList                    需要删除的收货地址的addressNo集合
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void removeAddress(Context context, ArrayList<AddressNoBean> addressList, AbstractActionCallbackListener<AddressListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (addressList.size() == 0) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.no_address_to_delete));
            return;
        }
        Map<String, String> params = new HashMap<String, String>();

        Map<String, ArrayList> param = new HashMap<>();
        param.put("address_list", addressList);
        Gson mGson = new Gson();
        String addrGson = mGson.toJson(param).toString();

        params.put("address_json", addrGson);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.REMOVE_ADDRESS_URL, params, AddressListBean.class, abstractActionCallbackListener, tagListener);

    }

    /**
     * 2023 设置默认收货地址
     *
     * @param context
     * @param addressNo
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void setDefaultAddress(Context context, String addressNo, AbstractActionCallbackListener<AddressListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(addressNo)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.address_no_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("address_no", addressNo);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.SET_DEFAULT_ADDRESS_URL, params, AddressListBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1031 新增收货地址
     *
     * @param context
     * @param addressInfo                    地址信息
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void addAddress(Context context, AddaddressRequestBean addressInfo, AbstractActionCallbackListener<AddressListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(addressInfo.consignee)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.consignee_can_not_null));
            return;
        }
        if (addressInfo.tel.split("-").length < 1 || TextUtils.isEmpty(addressInfo.tel.split("-")[0])) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_country_code));
            return;
        }
        if (addressInfo.tel.split("-").length < 2 || TextUtils.isEmpty(addressInfo.tel.split("-")[1])) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.phone_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.address)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.address_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.province_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_province));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.city_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_city));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.area_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_area));
            return;
        }

        Map<String, AddaddressRequestBean> param = new HashMap<String, AddaddressRequestBean>();
        param.put("add_info", addressInfo);
        Gson mGson = new Gson();
        String addrGson = mGson.toJson(param).toString();

        Map<String, String> params = new HashMap<String, String>();
        params.put("add_info_json", addrGson);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.ADD_ADDRESS_URL, params, AddressListBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1032 根据地址编号修改收货地址
     *
     * @param context
     * @param addressNo                      地址编号
     * @param addressInfo                    地址信息
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void updateAddressByNo(Context context, String addressNo, AddaddressRequestBean addressInfo, AbstractActionCallbackListener<AddressListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(addressNo)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.address_no_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.consignee)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.consignee_can_not_null));
            return;
        }
        if (addressInfo.tel.split("-").length < 1 || TextUtils.isEmpty(addressInfo.tel.split("-")[0])) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_country_code));
            return;
        }
        if (addressInfo.tel.split("-").length < 2 || TextUtils.isEmpty(addressInfo.tel.split("-")[1])) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.phone_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.address)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.address_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.province_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_province));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.city_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_city));
            return;
        }
        if (TextUtils.isEmpty(addressInfo.area_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_area));
            return;
        }

        Map<String, String> params = new HashMap<String, String>();
        Map<String, AddaddressRequestBean> param = new HashMap<String, AddaddressRequestBean>();
        param.put("add_info", addressInfo);
        Gson mGson = new Gson();
        String addrGson = mGson.toJson(param).toString();

        params.put("address_no", addressNo);
        params.put("add_info_json", addrGson);

        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.UPDATE_ADDRESS_URL, params, AddressListBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 2018 获取基础数据
     *
     * @param context
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getBaseData(Context context, String base_data_type_json, AbstractActionCallbackListener<BaseDataListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params.put("base_data_type_json", base_data_type_json);
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.GET_BASE_DATA_URL, params, BaseDataListBean.class, abstractActionCallbackListener, tagListener, false);
    }

    /**
     * 2018 获取所有基础数据
     *
     * @param context
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getBaseData(Context context, AbstractActionCallbackListener<BaseDataListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        getBaseData(context, "", abstractActionCallbackListener, tagListener);
    }

    /**
     * 2018 根据类别获取基础数据(省市区列表，送货方式列表，支付方式列表等）
     *
     * @param context
     * @param callbackListener 回调listener。注意：调用该方法时 new BaseDataCallBackListener构造方法中参数type必须和改方法中的typeName保持一致。
     *                         BaseDataCallBackListener(Context context，String type)
     *                         type值应该为BaseDataTypeNameBean中常量。具体意义参见BaseDataTypeNameBean
     * @param tagListener
     */
    public static void getBaseDataByType(String typeName, Context context, BaseDataCallBackListener callbackListener, RequestTagListener tagListener) {
        ArrayList<BaseDataInfoBean> dataList = callbackListener.getDataListFormSp(typeName);
        if (typeName.equals(BaseDataTypeNameBean.TYPE_CITY) && dataList != null && dataList.size() > 0) {
            callbackListener.getBaseDataListBean(dataList);
        } else {
            BaseDataTypesBean types = new BaseDataTypesBean();
            List<BaseDataTypeNameBean> list = new ArrayList<>();
            list.add(new BaseDataTypeNameBean(typeName));
            types.setTypes(list);
            Gson gson = new Gson();
            getBaseData(context, gson.toJson(types).toString(), callbackListener, tagListener);
        }
    }

    /**
     * 2026 获取功能介绍显示页地址
     *
     * @param context
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getIntroductionInfo(Context context, AbstractActionCallbackListener<IntroducitonBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_INTRODUCTION_URL, params, IntroducitonBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 2025 获取版本申明显示页地址
     *
     * @param context
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getAgreementInfo(Context context, AbstractActionCallbackListener<AgreementBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_USER_AGREEMENT_URL, params, AgreementBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1022 获取商品详情
     *
     * @param context
     * @param product_id                     商品id
     * @param post_id                        帖子id（可为空）
     * @param action                         商品明细入口（必须指定） 取值参考常量定义 商品明细入口
     *                                       1	首页活动
     *                                       2	帖子链接
     *                                       4	扫描条码
     *                                       5	购物车
     *                                       6	魔店
     * @param mstore_id                      魔店ID（可为空）
     * @param relation_id                    二维码关联ID（可为空）
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getProdDetailById(Context context, String product_id, String post_id, String action, String mstore_id, String relation_id, AbstractActionCallbackListener<ProdBaseInfoBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        //这里的数据校验是为了方法调用时，参数忘传。非用户输入校验
        if (TextUtils.isEmpty(product_id) || TextUtils.isEmpty(action)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.param_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("product_id", product_id);
        params.put("post_id", post_id);
        params.put("action", action);
        params.put("mstore_id", mstore_id);
        params.put("relation_id", relation_id);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_PROD_DETAIL_URL, params, ProdBaseInfoBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1025 加入购物车
     *
     * @param context
     * @param postID                         帖子id 可为空
     * @param product_info                   商品信息
     * @param mstoreID                       魔店id 可为空
     * @param realtionId                     关联id 可为空
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void addProdToCart(Context context, String postID, ProdCartItemRequestBean product_info, String mstoreID, String realtionId, AbstractActionCallbackListener<ShopCartListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(product_info.product_id)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getResources().getString(R.string.prodid_can_not_null));
            return;
        }
        if (product_info.order_qty < 1) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_ILLEGAL, context.getResources().getString(R.string.num_less_than_one));
            return;
        }
        for (int i = 0; i < product_info.prod_spec_list.size(); i++) {
            if (!TextUtils.isEmpty(product_info.prod_spec_list.get(i).prod_spec_id) && TextUtils.isEmpty(product_info.prod_spec_list.get(i).spec_detail_id)) {
                abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_ILLEGAL, String.format(context.getResources().getString(R.string.select_spec), product_info.prod_spec_list.get(i).prod_spec_name));
                return;
            }
        }
        Map<String, ProdCartItemRequestBean> product_info_param = new HashMap<>();
        product_info_param.put("product_info", product_info);
        Gson mGson = new Gson();
        String prodInfoJson = mGson.toJson(product_info_param).toString();

        Map<String, String> params = new HashMap<>();
        params.put("post_id", postID);
        params.put("wifi_id", "");
        params.put("mstore_id", mstoreID);
        params.put("realtion_id", realtionId);
        params.put("product_info_json", prodInfoJson);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.ADD_TO_SHOPCART_URL, params, ShopCartListBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1040 获取我的订单中order_staus类别的订单列表
     *
     * @param context
     * @param order_status                   订单状态类别。
     *                                       取值：参见Contants中订单状态过滤条件
     * @param order_no                       翻页时用。取值为前一页请求返回列表的最后一条订单的订单编号。取第一页数据时，传空值
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getOrderListByStatus(Context context, String order_status, String order_no, AbstractActionCallbackListener<OrderListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params.put("order_status", order_status);
        params.put("order_date_from", "");    //起始日期传空值表示取该账户下所有订单
        params.put("order_date_to", "");
        params.put("order_no", order_no);
        params.put("forward", Constants.FORWORD_DONW);
        params.put("fetch_count", Constants.FETCH_COUNT);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_ORDER_LIST_URL, params, OrderListBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1040 获取我的订单 --- 全部订单
     *
     * @param context
     * @param order_no                       翻页时用。取值为前一页请求返回列表的最后一条订单的订单编号。取第一页数据时，传空值
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getAllOrderList(Context context, String order_no, AbstractActionCallbackListener<OrderListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        getOrderListByStatus(context, Constants.ORDER_FILTER_TOTAL, order_no, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1040 获取我的订单 --- 待付款订单
     *
     * @param context
     * @param order_no                       翻页时用。取值为前一页请求返回列表的最后一条订单的订单编号。取第一页数据时，传空值
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getPendingpaymentOrderList(Context context, String order_no, AbstractActionCallbackListener<OrderListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        getOrderListByStatus(context, Constants.ORDER_FILTER_PENDING_PAY, order_no, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1040 获取我的订单 --- 待发货订单
     *
     * @param context
     * @param order_no                       翻页时用。取值为前一页请求返回列表的最后一条订单的订单编号。取第一页数据时，传空值
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getWaitForDeliveryOrderList(Context context, String order_no, AbstractActionCallbackListener<OrderListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        getOrderListByStatus(context, Constants.ORDER_FILTER_TOTAL, order_no, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1040 获取我的订单 --- 待评价订单
     *
     * @param context
     * @param order_no                       翻页时用。取值为前一页请求返回列表的最后一条订单的订单编号。取第一页数据时，传空值
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getWaitForEvaluationOrderList(Context context, String order_no, AbstractActionCallbackListener<OrderListBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        getOrderListByStatus(context, Constants.ORDER_FILTER_TOTAL, order_no, abstractActionCallbackListener, tagListener);
    }

    /**
     * 1041 根据订单编号获取订单详情
     *
     * @param context
     * @param orderNo          订单编号
     * @param callbackListener
     * @param tagListener
     */
    public static void getOrderDetails(Context context, String orderNo, AbstractActionCallbackListener<OrderDetailOrderBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(orderNo)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.order_no_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("order_no", orderNo);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_ORDER_DETAIL_URL, params, OrderDetailOrderBean.class, callbackListener, tagListener);
    }

    /**
     * 1089 获取帖子详情
     *
     * @param context
     * @param postId                         帖子id （必须项）
     * @param abstractActionCallbackListener
     * @param tagListener
     */
    public static void getPostDetail(Context context, String postId, AbstractActionCallbackListener<PostUserPostBean> abstractActionCallbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(postId)) {
            abstractActionCallbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.postid_can_not_null));
        }
        Map<String, String> params = new HashMap<>();
        params.put("post_id", postId);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_POST_DETAIL_URL, params, PostUserPostBean.class, abstractActionCallbackListener, tagListener);
    }

    /**
     * 2015 获取app下载链接
     *
     * @param context
     * @param callbackListener
     * @param tagListener
     */
    public static void getAppDownLink(Context context, AbstractActionCallbackListener<UrlAppDownLinkBean> callbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_APP_QRCODE_URL, params, UrlAppDownLinkBean.class, callbackListener, tagListener);
    }

    /**
     * 1046 获取商品指定等级评价列表
     *
     * @param context
     * @param product_id       商品id（必须项）
     * @param level            取值参见 常量定义 评价等级
     * @param rating_doc_no    评价项id
     * @param callbackListener
     * @param tagListener
     */
    public static void getProdEvalListByLevel(Context context, String product_id, String level, String rating_doc_no, AbstractActionCallbackListener<ProdEvaluationListBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(product_id)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prodid_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("product_id", product_id);
        params.put("level", level);
        params.put("rating_doc_no", rating_doc_no);
        params.put("forward", Constants.FORWORD_DONW);
        params.put("fetch_count", Constants.FETCH_COUNT);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_ORDER_APPRAISAL_URL, params, ProdEvaluationListBean.class, callbackListener, tagListener);
    }

    /**
     * 1046 获取商品评价列表 -- 好评列表
     *
     * @param context
     * @param product_id
     * @param rating_doc_no
     * @param callbackListener
     * @param tagListener
     */
    public static void getProdEvalListNice(Context context, String product_id, String rating_doc_no, AbstractActionCallbackListener<ProdEvaluationListBean> callbackListener, RequestTagListener tagListener) {
        getProdEvalListByLevel(context, product_id, Constants.PROD_EVAL_LEVEL_NICE, rating_doc_no, callbackListener, tagListener);
    }

    /**
     * 1046 获取商品评价列表  -- 所有等级评价列表
     *
     * @param context
     * @param product_id
     * @param rating_doc_no
     * @param callbackListener
     * @param tagListener
     */
    public static void getProdEvalListAll(Context context, String product_id, String rating_doc_no, AbstractActionCallbackListener<ProdEvaluationListBean> callbackListener, RequestTagListener tagListener) {
        getProdEvalListByLevel(context, product_id, Constants.PROD_EVAL_LEVEL_ALL, rating_doc_no, callbackListener, tagListener);
    }

    /**
     * 2021 收藏商品
     *
     * @param context
     * @param favoritesId      商品id（必须项）
     * @param relation_id      二维码关联id
     * @param callbackListener
     * @param tagListener
     */
    public static void addFavoriteProd(Context context, String favoritesId, String relation_id, AbstractActionCallbackListener callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(favoritesId)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prodid_can_not_null));
            return;
        }
        Map params = new HashMap<>();
        ArrayList<FavoritesInfoRequestBean> list = new ArrayList<>();
        list.add(new FavoritesInfoRequestBean(Constants.FAVORITE_TYPE_PROD, favoritesId));
        params.put("favorites_list_json", list);
        params.put("relation_id", relation_id);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.ADD_FAVORITE_URL, params, null, callbackListener, tagListener, false);
    }

    /**
     * 1088 取消已收藏商品
     *
     * @param context
     * @param favoritesId      商品id
     * @param callbackListener
     * @param tagListener
     */
    public static void removeFavoriteProd(Context context, String favoritesId, AbstractActionCallbackListener callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(favoritesId)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prodid_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("key_value", favoritesId);
        params.put("collection_type", Constants.FAVORITE_TYPE_PROD);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.CANCEL_FAVORITE_URL, params, null, callbackListener, tagListener, false);
    }

    /**
     * 1024 获取商品图文详情
     *
     * @param context
     * @param productId        商品id
     * @param callbackListener
     * @param tagListener
     */
    public static void getProdPicAndText(Context context, String productId, AbstractActionCallbackListener<ProdPicAndTextListBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(productId)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prodid_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("product_id", productId);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_PROD_TXET_PIC_URL, params, ProdPicAndTextListBean.class, callbackListener, tagListener);
    }

    /**
     * 1023 获取商品规格参数列表
     *
     * @param context
     * @param productId        商品id
     * @param callbackListener
     * @param tagListener
     */
    public static void getProdSpecParameterList(Context context, String productId, AbstractActionCallbackListener<ProdParametersListBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(productId)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prodid_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("product_id", productId);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_PROD_SPEC_URL, params, ProdParametersListBean.class, callbackListener, tagListener);
    }

    /**
     * 2019 获取我收藏的商品列表
     *
     * @param context
     * @param prodId           商品id
     * @param callbackListener
     * @param tagListener
     */
    public static void getMyFavoriteProdList(Context context, String prodId, AbstractActionCallbackListener<ProdListBean> callbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params.put("prod_id", prodId);
        params.put("forward", Constants.FORWORD_DONW);
        params.put("fetch_count", Constants.FETCH_COUNT);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GTE_FAROVITE_PROD_LIST_URL, params, ProdListBean.class, callbackListener, tagListener);
    }

    /**
     * 1042 根据订单号删除/取消订单
     *
     * @param context
     * @param orderNo          订单号
     * @param reason           取消理由
     * @param callbackListener
     * @param tagListener
     */
    public static void deleteOrderByNo(Context context, String orderNo, String reason, AbstractActionCallbackListener callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(orderNo)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.order_no_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("order_no", orderNo);
        params.put("reason", reason);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.DELETE_ORDER_BY_NO_URL, params, null, callbackListener, tagListener);
    }

    /**
     * 1042 根据订单号删除已完成订单。注：删除已完成订单无须输入reason。
     *
     * @param context
     * @param orderNo          订单号
     * @param callbackListener
     * @param tagListener
     */
    public static void deleteCompletedOrder(Context context, String orderNo, AbstractActionCallbackListener callbackListener, RequestTagListener tagListener) {
        deleteOrderByNo(context, orderNo, "", callbackListener, tagListener);
    }

    /**
     * 2024 确认收货
     *
     * @param context
     * @param orderNo          订单编号
     * @param callbackListener
     * @param tagListener
     */
    public static void comfirmReceipt(Context context, String orderNo, AbstractActionCallbackListener callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(orderNo)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.order_no_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("order_no", orderNo);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.ORDER_COMFIRM_RECEIPT_URL, params, null, callbackListener, tagListener);
    }

    /**
     * 1045 评价商品（只能逐个评价订单中商品）
     *
     * @param context
     * @param order_no         订单变化
     * @param order_detail_no  订单明细编号
     * @param product_id       商品编号
     * @param level            评分
     * @param memo             评价内容
     * @param callbackListener
     * @param tagListener
     */
    public static void EvalutionProd(Context context, String order_no, String order_detail_no, String product_id, String level, String memo, AbstractActionCallbackListener callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(order_no)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.order_no_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(order_detail_no)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.order_detail_no_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(product_id)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prodid_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(level)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getApplicationContext().getString(R.string.give_level));
            return;
        }
        if (TextUtils.isEmpty(memo)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getApplicationContext().getString(R.string.put_evaluation_content));
            return;
        }
        Map<String, String> params = new HashMap<>();
        Map<String, Object> ratingDetail = new HashMap<>();
        ratingDetail.put("order_no", order_no);
        ratingDetail.put("order_detail_no", order_detail_no);
        ratingDetail.put("product_id", product_id);
        ratingDetail.put("level", level);
        ratingDetail.put("memo", memo);
        List list = new ArrayList();
        list.add(ratingDetail);
        Map ratingJsonMap = new HashMap<>();
        ratingJsonMap.put("rating_detail", list);
        Gson mGson = new Gson();
        String rateGson = mGson.toJson(ratingJsonMap).toString();
        params.put("rating_detail_json", rateGson);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.EVALUATE_PROD_URL, params, null, callbackListener, tagListener);
    }

    /**
     * 1085 获取商品二维码
     *
     * @param context
     * @param prod_id          商品id
     * @param relation_id      商品关联id
     * @param callbackListener
     * @param tagListener
     */
    public static void getProdQrcode(Context context, String prod_id, String relation_id, AbstractActionCallbackListener<ProdQrInfoBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(prod_id)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prodid_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("prod_id", prod_id);
        params.put("relation_id", relation_id);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_PROD_QR_URL, params, ProdQrInfoBean.class, callbackListener, tagListener);
    }

    /**
     * 1064 二维码扫描结果解析
     *
     * @param context
     * @param scanData         扫描到的原始数据（必须项）
     * @param callbackListener
     * @param tagListener
     */
    public static void decodeQR(Context context, String scanData, AbstractActionCallbackListener<ScanQrDataBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(scanData)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.scan_data_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("scan_data", scanData);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.DECODE_QR_URL, params, ScanQrDataBean.class, callbackListener, tagListener);
    }

    /**
     * 2042 购物车商品结算
     *
     * @param context
     * @param prodDetailNoList 需要结算的商品列表
     * @param addressNo        送货地址（可为空）
     * @param transportWay     配送方式 （可为空）
     * @param callbackListener
     * @param tagListener
     */
    public static void cartProdSettle(Context context, ArrayList<String> prodDetailNoList, String addressNo, String transportWay, AbstractActionCallbackListener<SettlementInfoBean> callbackListener, RequestTagListener tagListener) {

        if (prodDetailNoList.size() < 1) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.selected_prod_null));
            return;
        }
        Map<String, String> params = new HashMap<String, String>();

        Gson mGson = new Gson();
        Map<String, ArrayList> typeListParam = new HashMap<>();
        typeListParam.put("product_info", prodDetailNoList);
        String typeListString = mGson.toJson(typeListParam).toString();

        params.put("product_info_json", typeListString);
        params.put("address_no", addressNo);
        params.put("transport_way", transportWay);

        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.SETTLE_CART_PROD_URL, params, SettlementInfoBean.class, callbackListener, tagListener);
    }

    /**
     * 12002 结算（仅适用于立即购买流程，不适应于购物车购买流程）
     *
     * @param productInfo 商品信息对象
     *                    入口参数示例：product_info_json：{"product_info":{"product_id":"J16080300000002","order_qty":"1","prod_spec_list":[{"prod_spec_id":"0001","prod_spec_name":"规格","spec_detail_id":"J16080300000644"}]}}
     * @param post_id
     * @param relation_id
     * @param address_no
     */
    static public void balanceNow(Context context, SettleProdRequestBean productInfo, String post_id, String relation_id, String address_no, AbstractActionCallbackListener<SettlementInfoBean> callbackListener, RequestTagListener tagListener) {
        if (null == productInfo) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.prod_info_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        Map prodInfoParam = new HashMap();
        prodInfoParam.put("product_info", productInfo);
        Gson gson = new Gson();

        params.put("product_info_json", gson.toJson(prodInfoParam));
        params.put("post_id", post_id);
        params.put("relation_id", relation_id);
        params.put("address_no", address_no);
        params = getParams_gson(params);

        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.SETTLE_PROD_URL, params, SettlementInfoBean.class, callbackListener, tagListener);
    }

    /**
     * 12003 立即购买下单（仅适用于立即购买流程，不适应于购物车购买流程）
     *
     * @param order_info
     */
    static public void creatOrderNow(Context context, OrderCreateNowOrderRequestBean order_info, AbstractActionCallbackListener<OrderCreateResultBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(order_info.shipping_method.shipping_method_no)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_shipping_method));
            return;
        }
        if (TextUtils.isEmpty(order_info.since_some_id) && TextUtils.isEmpty(order_info.address_no)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_address));
            return;
        }
        if (TextUtils.isEmpty(order_info.receive_date) && TextUtils.isEmpty(order_info.shipping_method.shipping_time_memo)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_receive_date));
            return;
        }
        if (parseInt(order_info.product_info.order_qty) < 0) {
            callbackListener.onFailure(ErrorEvent.PARAM_ILLEGAL, ApiLibApplication.mInstance.getString(R.string.num_less_than_one));
            return;
        }
        for (int i = 0; i < order_info.product_info.prod_spec_list.size(); i++) {
            if (!TextUtils.isEmpty(order_info.product_info.prod_spec_list.get(i).prod_spec_id) && TextUtils.isEmpty(order_info.product_info.prod_spec_list.get(i).spec_detail_id)) {
                callbackListener.onFailure(ErrorEvent.PARAM_ILLEGAL, String.format(context.getResources().getString(R.string.select_spec), order_info.product_info.prod_spec_list.get(i).prod_spec_name));
                return;
            }
        }

        Map orderInfoParam = new HashMap();
        orderInfoParam.put("order_info", order_info);
        Gson gson = new Gson();

        Map<String, String> params = new HashMap<>();
        params.put("order_info_json", gson.toJson(orderInfoParam));
        params.put("buy_user_id", ApiLibApplication.mInstance.mConfigInfoBean.getUserid());
        params.put("pay_scene", "");
        params.put("id_no", "");
        params.put("sign_pic", "");

        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.CREATE_ORDER_NOW, params, OrderCreateResultBean.class, callbackListener, tagListener);
    }

    /**
     * 验证是否登录
     *
     * @param callbackListener
     */
    public static void verifyIsLogin(AbstractActionCallbackListener callbackListener) {
        if (Constants.USER_TYPE_FREEMEN.equals(ApiLibApplication.mInstance.mConfigInfoBean.getUserType())) {
            callbackListener.onFailure(ErrorEvent.NETWORK_TOKEN_FAIL_EVENT, "");
        } else {
            callbackListener.onSuccess(null);
        }
    }

    /**
     * 11005 获取自提点列表
     *
     * @param context
     * @param addressId        自提点地址id。拉取第一页数据时传空值
     * @param callbackListener
     * @param tagListener
     */
    public static void getPickUpAddressList(Context context, String addressId,String transportway, AbstractActionCallbackListener<PickUpAddressBean> callbackListener, RequestTagListener tagListener) {
//        if (TextUtils.isEmpty(addressId)) {
//            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.address_id_can_not_null));
//            return;
//        }
        Map<String, String> params = new HashMap<>();
        params.put("since_some_id", addressId);
        params.put("forward", Constants.FORWORD_DONW);
        params.put("fetch_count", Constants.FETCH_COUNT);
        params.put("transportway",transportway);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_PICK_UP_ADDRESS_LIST_URL, params, PickUpAddressBean.class, callbackListener, tagListener);
    }

    /**
     * 11006 根据用户id获取用户护照入境等信息
     *
     * @param context
     * @param userId
     * @param callbackListener
     * @param tagListener
     */
    public static void getUserEntryInfo(Context context, String userId, AbstractActionCallbackListener<UserEntryBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(userId)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.userid_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("user_id", userId);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_USER_ENTRY_INFO_URL, params, UserEntryBean.class, callbackListener, tagListener);
    }

    /**
     * 11004 上传护照入境日等信息
     *
     * @param context
     * @param paramBean
     * @param callbackListener
     * @param tagListener
     */
    public static void uploadEntryInfo(Context context, UserEntryRequestBean paramBean, AbstractActionCallbackListener callbackListener, RequestTagListener tagListener) {

//        if (TextUtils.isEmpty(paramBean.passport_pic)) {
//            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.no_passport_pic));
//            return;
//        }
//        if (TextUtils.isEmpty(paramBean.customs_entry_pic)) {
//            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.no_entry_pic));
//            return;
//        }
        Map params = new HashMap();
        params.put("first_name", paramBean.first_name);
        params.put("last_name", paramBean.last_name);
        params.put("area", paramBean.area);
        params.put("passport_no", paramBean.passport_no);
        params.put("issue_date", paramBean.issue_date);
        params.put("expiry_date", paramBean.expiry_date);
        params.put("entry_date", paramBean.entry_date);
        params.put("departure_date", paramBean.departure_date);
        params.put("customs_entry_pic", paramBean.customs_entry_pic);
        params.put("passport_pic", paramBean.passport_pic);
        params.put("pic_type", paramBean.pic_type);
        params.put("ocr_code", paramBean.ocr_code);

        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.UPLOAD_ENTRY_INFO_URL, params, null, callbackListener, tagListener);
    }

    /**
     * 2033 根据订单号获取微信支付的支付参数
     *
     * @param context
     * @param orderNo
     * @param callbackListener
     * @param tagListener
     */
    public static void getWXpayParameter(Context context, String orderNo, AbstractActionCallbackListener<WXpayParameterBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(orderNo)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.order_no_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("order_no", orderNo);
        params.put("app_inner_no", ApiLibApplication.mInstance.mConfigInfoBean.getApp_inner_no());
        params.put("pay_method", Constants.PAY_TYPE_WECHAT);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_WXPAY_PARAMETER_URL, params, WXpayParameterBean.class, callbackListener, tagListener);
    }

    /**
     * 2034  根据订单号获取支付宝支付的支付参数
     *
     * @param context
     * @param orderNo
     * @param callbackListener
     * @param tagListener
     */
    public static void getAlipayParameter(Context context, String orderNo, AbstractActionCallbackListener<AlipayParameterBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(orderNo)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.order_no_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("order_no", orderNo);
        params.put("app_inner_no", ApiLibApplication.mInstance.mConfigInfoBean.getApp_inner_no());
        params.put("pay_method", Constants.PAY_TYPE_ALIPAY);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_ALIPAY_PARAMETER_URL, params, AlipayParameterBean.class, callbackListener, tagListener);
    }

    /**
     * 2012 获取个人基本信息
     *
     * @param context
     * @param callbackListener
     * @param tagListener
     */
    public static void getPersonalInfo(Context context, AbstractActionCallbackListener<PersonalInfoBean> callbackListener, RequestTagListener tagListener) {

        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.GET_PERSONAL_INFO_URL, params, PersonalInfoBean.class, callbackListener, tagListener, false);
    }

    /**
     * 11008 获取我的购物车列表
     *
     * @param context
     * @param callbackListener
     * @param tagListener
     */
    public static void getCartWithStore(Context context, AbstractActionCallbackListener<ShopCartBean> callbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequest(context, HttpConstants.GET_SHOPCART_WITH_STORE, params, ShopCartBean.class, callbackListener, tagListener, false);
    }

    /**
     * 11007 取消已付款订单并退款
     *
     * @param context
     * @param orderNo          订单号
     * @param callbackListener
     * @param tagListener
     */
    public static void deleteOrderByNo(Context context, String orderNo, AbstractActionCallbackListener<OrderDeleteBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(orderNo)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.order_no_can_not_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("order_no", orderNo);
        params.put("reason", "");
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.CANCEL_PAID_ORDER, params, OrderDeleteBean.class, callbackListener, tagListener);
    }

    /**
     * 2022  商品转发
     *
     * @param context
     * @param product_id       商品id
     * @param message          转发内容
     * @param repost_type      转发类别
     * @param relation_id      二维码关联id
     * @param callbackListener
     * @param tagListener
     */
    public static void productShare(Context context, String product_id, String message, String repost_type, String relation_id, AbstractActionCallbackListener<ProductShareBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(repost_type)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_type));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("product_id", product_id);
        params.put("message", message);
        params.put("repost_type", repost_type);
        params.put("relation_id", relation_id);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.PRODUCT_SHARE_URL, params, ProductShareBean.class, callbackListener, tagListener);
    }

    /**
     * 1020  帖子转发
     *
     * @param context
     * @param parent_post_id   原帖子id
     * @param message          转发内容
     * @param repost_type      转发类别
     * @param callbackListener
     * @param tagListener
     */
    public static void postShare(Context context, String parent_post_id, String message, String repost_type, AbstractActionCallbackListener<PostShareBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(repost_type)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.please_select_type));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("parent_post_id", parent_post_id);
        params.put("message", message);
        params.put("repost_type", repost_type);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.POST_SHARE_URL, params, PostShareBean.class, callbackListener, tagListener);
    }

    /**
     * 6015  获取我的可用优惠券
     *
     * @param context
     * @param goods_info       商品列表信息
     * @param callbackListener
     * @param tagListener
     */
    public static void getUsefulCouponRequest(Context context, List<UsefulCouponRequestBean> goods_info, AbstractActionCallbackListener<UsefulCouponBean> callbackListener, RequestTagListener tagListener) {
        if (goods_info.size() < 1) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.selected_prod_null));
            return;
        }
        Map<String, Object> requestParam = new HashMap<>();
        requestParam.put("goods_info", goods_info);
        Map<String, String> params = getParams_gson(requestParam);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.USEFUL_COUPON_URL, params, UsefulCouponBean.class, callbackListener, tagListener);
    }

    /**
     * 11009 中银支付 取的支付的URL
     *
     * @param context
     * @param order_no         订单编号
     * @param callbackListener
     * @param tagListener
     */
    public static void getUnionPayParams(Context context, String order_no, AbstractActionCallbackListener<UnionPayParamsBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(order_no)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.order_info_can_not_null));
        }
        Map<String, String> params = new HashMap<>();
        params.put("order_number", order_no);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_UNIONPAY_PARAM_URL, params, UnionPayParamsBean.class, callbackListener, tagListener);
    }

    /**
     * @throws
     * @author: liuting
     * @date: 2017/5/12 15:48
     * @MethodName: updateAccountInfo
     * @Description: 1049 修改用户信息
     * @param: context                        上下文 Context
     * @param: updateAccountInfoRequestBean   1049接口入口参数
     * @param: callbackListener              请求回调
     * @param: tagListener                   RequestTagListener
     * @return: void
     */
    public static void updateAccountInfo(Context context, UpdateAccountInfoRequestBean updateAccountInfoRequestBean, AbstractActionCallbackListener<UpdateAccountInfoBean> callbackListener, RequestTagListener tagListener) {

        if (TextUtils.isEmpty(updateAccountInfoRequestBean.head_photo_binary_data)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.head_photo_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(updateAccountInfoRequestBean.user_nick_name)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.nickname_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(updateAccountInfoRequestBean.sex)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.sex_can_not_null));
            return;
        }
        if (TextUtils.isEmpty(updateAccountInfoRequestBean.user_name)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.real_name_can_not_null));
            return;
        }

        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("head_photo_binary_data", updateAccountInfoRequestBean.head_photo_binary_data);
        requestParam.put("user_nick_name", updateAccountInfoRequestBean.user_nick_name);
        requestParam.put("sex", updateAccountInfoRequestBean.sex);
        requestParam.put("id_no", updateAccountInfoRequestBean.id_no);
        requestParam.put("customs_flag", updateAccountInfoRequestBean.customs_flag);
        requestParam.put("user_name", updateAccountInfoRequestBean.user_name);
        Map<String, String> params = getParams_gson(requestParam);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.UPDATE_ACCOUNT_INFO_URL, params, UpdateAccountInfoBean.class, callbackListener, tagListener);
    }

    /**
     * 1077 音频数据解析
     *
     * @param context          Context
     * @param scanData         扫描到的原始数据（必须项）
     * @param callbackListener 请求回调
     * @param tagListener      RequestTagListener
     */
    public static void decodeListenData(Context context, String scanData, AbstractActionCallbackListener<ScanQrDataBean> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(scanData)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, ApiLibApplication.mInstance.getString(R.string.listen_data_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("scan_data", scanData);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.DECODE_LISTEN_DATA_URL, params, ScanQrDataBean.class, callbackListener, tagListener);
    }

    /**
     * @author:fengguowei
     * @date:2017/11/20
     * @param: context Context
     * @param:callbackListener回调接口
     * @param:tagListener 请求监听
     * @description: 获取帮助h5页面 使用通乐反馈接口
     */
    public static void doHelp(Context context, AbstractActionCallbackListener<HelpInfoBean> callbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params, true);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.HELP_SETTING_URL, params, HelpInfoBean.class, callbackListener, tagListener);
    }

    /**
     * 61008  获取绑定的导游信息
     *
     * @author:fengguowei
     * @date:2017/11/20
     * @param: context Context
     * @param:callbackListener回调接口
     * @param:tagListener 请求监听
     * @description: 获取绑定的导游信息
     */
    public static void getBindedGuide(Context context, AbstractActionCallbackListener<GuideInfoBean> callbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.GET_GUIDE_INFO_URL, params, GuideInfoBean.class, callbackListener, tagListener);
    }

    /**
     * 61010  绑定导游
     *
     * @author:fengguowei
     * @date:2017/11/20
     * @param: context Context
     * @param:callbackListener回调接口
     * @param:tagListener 请求监听
     * @description: 绑定导游
     */
    public static void bindGuide(Context context, String guideCode, AbstractActionCallbackListener<Object> callbackListener, RequestTagListener tagListener) {
        if (TextUtils.isEmpty(guideCode)) {
            callbackListener.onFailure(ErrorEvent.PARAM_NULL, context.getString(R.string.guide_code_null));
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("guide_id", guideCode);
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.BIND_GUIDE__URL, params, null, callbackListener, tagListener);
    }

    /**
     * 61009  解绑导游
     *
     * @author:fengguowei
     * @date:2017/11/20
     * @param: context Context
     * @param:callbackListener回调接口
     * @param:tagListener 请求监听
     * @description: 解绑导游
     */
    public static void unBindGuide(Context context, AbstractActionCallbackListener<Object> callbackListener, RequestTagListener tagListener) {
        Map<String, String> params = new HashMap<>();
        params = getParams_gson(params);
        RequestHelper.getmInstance().sendPostRequestWithDialog(context, HttpConstants.UNBIND_GUIDE__URL, params, null, callbackListener, tagListener);
    }

}
