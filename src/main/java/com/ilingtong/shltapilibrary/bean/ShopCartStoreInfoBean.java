package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/10/20.
 * mail: wuqian@ilingtong.com
 * Description: 购物车魔店详情
 * use by 11008
 */
public class ShopCartStoreInfoBean implements Serializable {
    public String mstore_id;    //	StoreID
    public String mstore_name;    //	Store名称
    public String mstore_pic_url;    //	Store ICON URL
    public String mstore_html_url;    //	Store HTML页面URL
    public double mstore_tax_limit;    //	Store退税额度

}
