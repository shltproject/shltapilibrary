package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description: 订单详情类
 * 共通块9007
 * use by 1040 获取我的订单列表(最外层)
 */
public class OrderInfoBean implements Serializable {
    public String order_no;
    public double amount;          //订单金额
    public double fee_amount;    //运费总额
    public String order_time;    //下单时间
    public String status;    //订单状态
    public String pay_method;   //支付方式
    public String modify_flag;   //订单是否可变更
    public String cancel_flag;   //订单是否可修改
    public String evaluate_flag;   //订单是否可评价
    public double goods_amount;       // 商品总额
    public double tariff;     // 关税
    public double pay_amount;     //  应付金额
    public String vouchers_name;     // 优惠券名称
    public double money;     //  优惠金额
    public String use_conditions;     //  优惠券使用条件
    public int goods_point;     //购买商品所需积分
    public ArrayList<OrderDetailInfoBean> prod_detail;  //订单商品明细列表
    public AddressPickupInfoBean since_some_info;  //自提点地址
    public String receive_date;  //自提时间
    public String tax_flg;  //是否退税区分
    public String receive_time;  //自提时间 时间段
}
