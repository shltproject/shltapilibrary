package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description:自提点地址信息
 * use by 11005 json最外层
 */
public class PickUpAddressBean implements Serializable {
    public PickUpAddressListBean since_some_info;
}
