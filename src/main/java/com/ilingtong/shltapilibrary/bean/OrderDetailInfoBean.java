package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description:订单商品明细列表信息
 * use by 1040
 */
public class OrderDetailInfoBean implements Serializable {
    public String order_detail_no;
    public String prod_pic_url;  //商品缩略图
    public String prod_id;
    public String prod_name;
    public double price;
    public int quantity;   //数量
    public ArrayList<ProdSpecListInfoBean> prod_spec_list;
    public String country_pic_url;
    public String import_info_desc;
    public String transfer_fee_desc ;
    public String tariff_desc;
    public String import_goods_flag;
    public String customs_flag;
    public String goods_eva_flag;  //商品可评价标志
    public int refund_qty;    //可退数量
    public int point;     //商品兑换所需积分
}
