package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/30.
 * mail: wuqian@ilingtong.com
 * Description: 微信支付参数对象
 * use by 2033
 */
public class WXpayParameterBean implements Serializable {
    public WXpayParameterInfoBean wexin_pay_info;
}
