package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description:订单详情中收货人地址信息。
 * use by 1041获取订单详情接口
 */
public class OrderDetailAddressBean implements Serializable {
    public String address_no;
    public String consignee;
    public String tel;
    public String province;
    public String area;
    public String address;
}
