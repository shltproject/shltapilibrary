package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/30.
 * mail: wuqian@ilingtong.com
 * Description:微信支付参数详情
 * use by 2033
 */
public class WXpayParameterInfoBean implements Serializable{
    public String app_id;
    public String partner_id;
    public String prepay_id;
    public String package_name;
    public String nonce_str;
    public String timestamp;
    public String sign;
}
