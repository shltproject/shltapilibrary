package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description:不同规格组合下对应的库存和价格。即：每一种商品的价格和库存
 * use by 1022获取商品详情
 */
public class ProdSpecStockBean implements Serializable{
    public String spec_detail_id1;//规格1明细ID
    public String spec_detail_id2;//规格2明细ID
    public int spec_stock_qty;//剩余库存
    public double spec_price;//规格价格
}
