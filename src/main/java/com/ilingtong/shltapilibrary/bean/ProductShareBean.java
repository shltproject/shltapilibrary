package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Package:com.ilingtong.shltapilibrary.bean
 * author:liuting
 * Date:2016/10/31
 * Desc:商品转发信息类，该类包含通过转发返回的商品信息
 * used by 2022
 */

public class ProductShareBean implements Serializable {
    public String prod_link_url;//转发后商品链接url
    public String prod_image_url;//转发后商品缩略图url
    public String magazine_title;//转发后杂志标题
    public String wx_profile;//转发后微信说明
}
