package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 物流信息
 * use by 1041
 */
public class OrderDetailLogisticsBean implements Serializable {
    public String shipping_company;
    public String shipping_no;
    public String shipping_time;
}
