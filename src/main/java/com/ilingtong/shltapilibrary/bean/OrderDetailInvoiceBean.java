package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 发票信息
 * use by 1041
 */
public class OrderDetailInvoiceBean implements Serializable {
    public String invoice_type_no;
    public String invoice_type_name;
    public String invoice_content_flag;
    public String invoice_content_flag_name;
    public String invoice_title;
}
