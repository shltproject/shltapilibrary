package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description: 提交订单时选择的发票信息
 * use by 1029 入口参数invoice_info的实体
 * use by 12003 入口参数invoice_info的实体
 */
public class InvoiceInfoRequestBean implements Serializable {
    //发票类型编号
    public String invoice_type_no="";
    //发票内容区分
    public String invoice_content_flag="";
    //发票抬头备注
    public String invoice_title="";
}
