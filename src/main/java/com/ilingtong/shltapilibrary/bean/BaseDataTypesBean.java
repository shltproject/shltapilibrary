package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/29.
 * mail: wuqian@ilingtong.com
 * Description:基础数据列表  用于2018接口入口参数对象化。
 */
public class BaseDataTypesBean implements Serializable{
    private List<BaseDataTypeNameBean> types;

    public List<BaseDataTypeNameBean> getTypes() {
        return types;
    }

    public void setTypes(List<BaseDataTypeNameBean> types) {
        this.types = types;
    }
}
