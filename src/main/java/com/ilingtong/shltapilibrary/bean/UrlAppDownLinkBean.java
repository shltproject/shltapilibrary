package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: app下载地址
 * use by 2015
 */
public class UrlAppDownLinkBean implements Serializable {
 public String app_qr_code_url;
}
