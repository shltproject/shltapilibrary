package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/12.
 * mail: wuqian@ilingtong.com
 * Description: 订单信息（新增订单）
 * use by 1029 新增订单
 */
public class OrderInfoCreateBean implements Serializable{
    public String order_no;  //订单编号
    public String order_status;  //订单状态
}
