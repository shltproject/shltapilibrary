package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Package:com.ilingtong.shltapilibrary.bean
 * author:liuting
 * Date:2016/11/3
 * Desc:商品信息
 * use by 6015（入口参数）
 */

public class UsefulCouponRequestBean implements Serializable{
    public String goods_id;//商品编号
    public String spec_id1;//规格ID1
    public String spec_id2;//规格ID2
    public String buy_count;//购买数量
}
