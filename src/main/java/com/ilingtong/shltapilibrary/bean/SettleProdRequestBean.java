package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description:商品立即购买结算入口参数product_info实体
 * use by 12002
 */
public class SettleProdRequestBean implements Serializable {
    public String product_id;
    public String order_qty;
    public ArrayList<ProdSpecListInfoBean> prod_spec_list;
}
