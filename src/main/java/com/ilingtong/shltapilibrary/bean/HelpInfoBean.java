package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * @ClassName:HelpInfo
 * @author:fengguowei
 * @date:2017/11/20
 * @Description:帮助接口实体信息类
 */

public class HelpInfoBean implements Serializable {
    private String title_text;
    private String contact_url;

    public String getTitle_text() {
        return title_text;
    }

    public String getContact_url() {
        return contact_url;
    }

    @Override
    public String toString() {
        return "title_text:" + title_text + "\r\n" +
                "contact_url:" + contact_url;
    }
}
