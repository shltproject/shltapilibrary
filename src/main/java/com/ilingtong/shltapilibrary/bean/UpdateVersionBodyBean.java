package com.ilingtong.shltapilibrary.bean;

/**
 * Created by wuqian on 2016/8/24.
 * mail: wuqian@ilingtong.com
 * Description:1001接口 版本更新返回参数
 */
public class UpdateVersionBodyBean {
    public String has_update;    //(是否有更新
    public String force_update;    //是否强制更新
    public String recently_version_no;    //新版本号
    public String recently_version_tips;    //APP新版本描述
    public String recently_version_link;    //APP新版本下载链接URL
}
