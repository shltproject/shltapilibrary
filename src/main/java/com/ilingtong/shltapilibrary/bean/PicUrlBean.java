package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description:图片地址url。
 * use by 1022
 */
public class PicUrlBean implements Serializable{
    public String pic_url;
}
