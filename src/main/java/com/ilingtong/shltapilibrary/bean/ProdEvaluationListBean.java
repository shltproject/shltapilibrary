package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 商品评价列表
 * use by 1046 获取商品评价 json最外层
 */
public class ProdEvaluationListBean implements Serializable {
   public int data_total_count;
   public ArrayList<ProdEvaluationInfoBean> product_rating_list;
}
