package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/10/10.
 * mail: wuqian@ilingtong.com
 * Description:个人基本信息
 * use by 2012接口
 */
public class PersonalInfoBean implements Serializable {
    public String user_id;
    public String user_name;
    public String user_nick_name;
    public String user_phone;
    public String user_photo_url;
    public String user_sex;
    public String user_id_no;
    public String user_sales_point;
    public String user_rebate_point;
    public String user_account_balance;
    public String user_coupons;
    public String user_fans;
    public String user_follows;
    public String user_qr_code_url;   //个人二维码
    public String user_customs_flag;
    public String user_point;
}
