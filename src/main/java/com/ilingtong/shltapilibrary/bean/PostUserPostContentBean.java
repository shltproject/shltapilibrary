package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description:帖子内容实体
 * use by 1089 帖子详情
 */
public class PostUserPostContentBean implements Serializable {
    public String pic_url; //图片
    public String pic_memo; //图片说明
    public String related_type; //图文关联对象类别。
    public String object_id;  //类别下对象id
    public String relation_id;
    public String coupon_flag;    //团购商品标识
}
