package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description:版权申报页的标题和h5地址
 * use by 2025
 */
public class AgreementBean implements Serializable{
    public String title_text;
    public String user_agreement_url;
}
