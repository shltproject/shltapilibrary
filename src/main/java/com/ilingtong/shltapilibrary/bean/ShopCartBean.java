package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/10/20.
 * mail: wuqian@ilingtong.com
 * Description:购物车对象
 * json最外层
 * use by 11008
 */
public class ShopCartBean implements Serializable {
    public ShopCartInfoBean my_shopping_cart;
}
