package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description: 用户入境，护照等信息详情
 * use by 11006
 */
public class UserEntryInfoBean implements Serializable {
    public String first_name;     //	姓
    public String last_name;     //	名
    public String area;     //	国家或地区
    public String passport_no;     //	护照
    public String birth;     //	出生日
    public String expiry_date;     //	到期日
    public String entry_date;     //	入境日
    public String departure_date;     //	离境日
    public String customs_entry_url;     //	海关手续
    public String passport_url;     //	护照照片
    public String pic_type;     //	照片区分
    public ArrayList ocr_code;     //	OCR码
    public String upd_md5;     //	更新标识

}
