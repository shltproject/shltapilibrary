package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/9.
 * mail: wuqian@ilingtong.com
 * Description:我的购物车列表
 * use by 1026接口
 * use by 1025接口
 */
public class ShopCartListBean implements Serializable{
    public ArrayList<ShopCartListInfoBean> my_shopping_cart;  //1026返回列表
    public ArrayList<ShopCartListInfoBean> this_shopping_cart;  //1025返回列表
}
