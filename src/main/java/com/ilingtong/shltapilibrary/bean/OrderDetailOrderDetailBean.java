package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 订单详情类订单订单明细。（与订单列表中订单明细字段有差异）
 * use by 1041获取订单详情接口
 */
public class OrderDetailOrderDetailBean implements Serializable {
    public String order_detail_no;
    public String prod_pic_url;
    public String prod_id;
    public String post_id;
    public String prod_name;
    public String price;
    public String quantity;
    public String amount;
    public String fee_amount;
    public ArrayList<ProdSpecListInfoBean> prod_spec_list;
    public String country_pic_url;
    public String import_info_desc;
    public String transfer_fee_desc;
    public String tariff_desc;
    public String import_goods_flag;
    public String goods_eva_flag;  //可评价标志 add at 2016/04/22 （之前漏加）
    public String relation_id;  //add at 2016/04/07
}
