package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description: 商品详细信息实体
 * 共通块9005
 * use by 1022 获取商品详情
 */
public class ProdInfoBean implements Serializable{
    public String prod_id;
    public String prod_name;
    public String prod_resume;
    public ArrayList<PicUrlBean> prod_pic_url_list;
    public String prod_status;
    public String mstore_id;
    public String mstore_name;
    public double price;
    public double show_price;  //原价
    public int stock_qty;
    public ArrayList<ProdSpecTypeInfoBean> prod_spec_list;
    public String good_rating_percent;
    public String points_rule;
    public String prod_favorited_by_me;
    public String trade_prod_favorited_by_me;
    public String relation_id;
    public String country_pic_url;
    public String import_info_desc;
    public String transfer_fee_desc;
    public String tariff_desc;
    public String import_goods_flag;
    public String sender;
    public String goods_return_info;
    public String point_rule_url;
    public String point_rule_title;
    public ArrayList<ProdSpecStockBean> stock_list;//库存列表
    public String price_interval;  //价格区间
    public String wx_profile;  //微信简介
    public int buy_limit_qty;  //限购数量
    public int need_point;   //购买商品需要的积分
    public String goods_type; //商品类别标识
    public String discount_rate; //折扣率
    public String tax_flg;  //商品退税区分
    public String tax_desc_url; //退税说明url
}
