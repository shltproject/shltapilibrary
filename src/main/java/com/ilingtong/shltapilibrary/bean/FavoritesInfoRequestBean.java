package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 收藏对象信息
 * use by 2021接口入口参数favorites_list_json的列表项对象
 */
public class FavoritesInfoRequestBean implements Serializable {
    public String collection_type;  //收藏类别 如：商品，帖子，会员等等
    public String key_value;   //类别下具体对象的id 如：商品id，帖子id，会员id等等

    public FavoritesInfoRequestBean() {
    }
    public FavoritesInfoRequestBean(String collection_type, String key_value) {
        this.collection_type = collection_type;
        this.key_value = key_value;
    }
}
