package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description:
 * use by 1029 入口参数shipping_method对象。
 * use by 12003 入口参数shipping_method对象
 */
public class ShippingInfoReuqestBean implements Serializable {
    public String shipping_method_no = "";
    public String shipping_time_memo = "";

    public ShippingInfoReuqestBean() {
    }

    public ShippingInfoReuqestBean(String shipping_method_no, String shipping_time_memo) {
        this.shipping_method_no = shipping_method_no;
        this.shipping_time_memo = shipping_time_memo;
    }
}
