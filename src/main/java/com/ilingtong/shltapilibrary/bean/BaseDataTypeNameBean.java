package com.ilingtong.shltapilibrary.bean;

/**
 * Created by wuqian on 2016/3/29.
 * mail: wuqian@ilingtong.com
 * Description:基础数据  用于2018接口入口参数对象化。
 */
public class BaseDataTypeNameBean {
    public static String TYPE_CITY = "1";   //	省市区列表
    public static String TYPE_PAY = "2";   //	支付方式列表
    public static String TYPE_DELIVERY = "3";   //	配送方式列表
    public static String TYPE_GROUP_PAY = "9";   //	团购券支付方式列表
    public static String TYPE_DELIVERY_TIME = "6";  //配送时间列表

    private String data_type;  //基础数据类型

    public BaseDataTypeNameBean(String data_type) {
        this.data_type = data_type;
    }
}
