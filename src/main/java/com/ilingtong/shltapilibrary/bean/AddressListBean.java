package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: wuqian
 * Date: 2016/9/12
 * Time: 22:05
 * Email:
 * Desc: 地址信息类，该类包含通过网络返回的地址列表数据
 * use by 1030 接口
 * use by 2023 设置默认收货地址
 * use by 1033 删除收货地址
 */
public class AddressListBean implements Serializable {
    public ArrayList<AddressInfoBean> my_address_list;

    @Override
    public String toString() {
        return "my_address_list:" + my_address_list;
    }
}
