package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/9.
 * mail: wuqian@ilingtong.com
 * Description: 产品规格信息
 * use by 1026接口
 * use by 1040获取我的订单列表
 * use by 1025 （入口参数）
 * use by 11002 （入口参数）spec_detail_name不用传
 * use by 12003 （入口参数）spec_detail_name不用传
 * use by 11008接口
 * 该类为选中的规格信息.一个规格id下只对应一个规格明细id.用于购物车,订单,已选择规格.
 */
public class ProdSpecListInfoBean implements Serializable{
    public String prod_spec_id;  //规格id
    public String prod_spec_name;  //规格名称 如：颜色
    public String spec_detail_id;  //规格明细id
    public String spec_detail_name;  //规格明细名称 如：白色
}
