package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/10/20.
 * mail: wuqian@ilingtong.com
 * Description:购物车详情 包括魔店列表和所选购商品列表
 * use by 11008
 */
public class ShopCartInfoBean implements Serializable{
    public List<ShopCartStoreInfoBean> mstore_list;  //魔店列表
    public List<ShopCartProdListInfoBean> prod_list;  //选购的商品列表
}
