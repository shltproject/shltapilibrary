package com.ilingtong.shltapilibrary.bean;

import android.content.SharedPreferences;
import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/8/9.
 * mail: wuqian@ilingtong.com
 * Description:配置信息
 */
public class ConfigInfoBean implements Serializable {
//    private String packageName;  //当前project的包名

//    private String ifRemberKeyName;   //是否记住了密码SP存储KeyName
//    private String loginUseridKeyName;  //登录用户名SP存储KeyName
//    private String loginPwdKeyName;  //登录密码SP存储KeyName
//    private String ifAutoLoginKeyName;  //是否自动登录SP存储keyName
//    private String SHARE_SDK_ID;//sharesdk注册平台id
//    private String WECHAT_APPID;//微信平台appid
//    private String WECHAT_SECRET;//微信平台密钥
//
//    private String SINA_APPKEY;//新浪平台app_key
//    private String SINA_APPSECRET;//新浪平台密钥
//    private String SINA_REDIRECT;//新浪平台回调地址

 //   private boolean isLoin;  //是否已登录
    private String app_inner_no;
    private String userid;
    private String token;
    private String userType;   //用户区分
    private String loginActity;    //登录activity的地址。用于当token失效后跳转到主程序的登录页
    private SharedPreferences sp;

    /**
     * 构造方法 当configInfoBean被回收时调用该方法获取数据
     *
     * @param sp 存储configInfo的sharedpreference
     */
    public ConfigInfoBean(SharedPreferences sp) {
        this.sp = sp;
    }

    /**
     * 在主项目的application中必须调用该方法初始化版本号
     *
     * @param app_inner_no
     */
    public void initConfigInfo(String app_inner_no, String loginActity) {
        setApp_inner_no(app_inner_no);
        setLoginActity(loginActity);
    }

    /**
     * 登录成功调用该方法保存userid和token。设置islogin为true
     *
     * @param userid
     * @param token
     */
    public void loginSccussSettings(String userid, String token,String userType) {
        setUserid(userid);
        setToken(token);
        setUserType(userType);
    }

    public String getLoginActity() {
        if (TextUtils.isEmpty(loginActity)) {
            loginActity = sp.getString("loginActity", "");
        }
        return loginActity;
    }

    public void setLoginActity(String loginActity) {
        this.loginActity = loginActity;
        sp.edit().putString("loginActity", loginActity).commit();
    }
    public String getApp_inner_no() {
        if (TextUtils.isEmpty(app_inner_no)) {
            app_inner_no = sp.getString("app_inner_no", "");
        }
        return app_inner_no;
    }

    public void setApp_inner_no(String app_inner_no) {
        this.app_inner_no = app_inner_no;
        sp.edit().putString("app_inner_no", app_inner_no).commit();
    }
    public String getUserid() {
        if (TextUtils.isEmpty(userid)) {
            userid = sp.getString("userid", "");
        }
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
        sp.edit().putString("userid", userid).commit();
    }

    public String getToken() {
        if (TextUtils.isEmpty(token)) {
            token = sp.getString("token", "");
        }
        return token;
    }

    public void setToken(String token) {
        this.token = token;
        sp.edit().putString("token", token).commit();
    }

    public String getUserType() {
        if (TextUtils.isEmpty(userType)){
            userType = sp.getString("userType","");
        }
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
        sp.edit().putString("userType",userType);
    }
    //    public boolean isLoin() {
//        return isLoin;
//    }
//
//    public void setLoin(boolean loin) {
//        isLoin = loin;
//    }

 //    public String getPackageName() {
//        if (TextUtils.isEmpty(packageName)) {
//            packageName = sp.getString("packageName", "");
//        }
//        return packageName;
//    }
//
//    public void setPackageName(String packageName) {
//        this.packageName = packageName;
//        sp.edit().putString("packageName", packageName).commit();
//    }

//    public String getIfRemberKeyName() {
//        if (TextUtils.isEmpty(ifRemberKeyName)) {
//            ifRemberKeyName = sp.getString("ifRemberKeyName", "");
//        }
//        return ifRemberKeyName;
//    }
//
//    public void setIfRemberKeyName(String ifRemberKeyName) {
//        this.ifRemberKeyName = ifRemberKeyName;
//        sp.edit().putString("ifRemberKeyName", ifRemberKeyName).commit();
//    }
//
//    public String getLoginUseridKeyName() {
//        if (TextUtils.isEmpty(loginUseridKeyName)) {
//            loginUseridKeyName = sp.getString("loginUseridKeyName", "");
//        }
//        return loginUseridKeyName;
//    }
//
//    public void setLoginUseridKeyName(String loginUseridKeyName) {
//        this.loginUseridKeyName = loginUseridKeyName;
//        sp.edit().putString("loginUseridKeyName", loginUseridKeyName).commit();
//    }
//
//    public String getLoginPwdKeyName() {
//        if (TextUtils.isEmpty(loginPwdKeyName)) {
//            loginPwdKeyName = sp.getString("loginPwdKeyName", "");
//        }
//        return loginPwdKeyName;
//    }
//
//    public void setLoginPwdKeyName(String loginPwdKeyName) {
//        this.loginPwdKeyName = loginPwdKeyName;
//        sp.edit().putString("loginPwdKeyName", loginPwdKeyName).commit();
//    }
//
//    public String getIfAutoLoginKeyName() {
//        if (TextUtils.isEmpty(ifAutoLoginKeyName)) {
//            ifAutoLoginKeyName = sp.getString("ifAutoLoginKeyName", "");
//        }
//        return ifAutoLoginKeyName;
//    }
//
//    public void setIfAutoLoginKeyName(String ifAutoLoginKeyName) {
//        this.ifAutoLoginKeyName = ifAutoLoginKeyName;
//        sp.edit().putString("ifAutoLoginKeyName", ifAutoLoginKeyName).commit();
//    }
//
//    public String getSHARE_SDK_ID() {
//        if (TextUtils.isEmpty(SHARE_SDK_ID)) {
//            SHARE_SDK_ID = sp.getString("SHARE_SDK_ID", "");
//        }
//        return SHARE_SDK_ID;
//    }
//
//    public void setSHARE_SDK_ID(String SHARE_SDK_ID) {
//        this.SHARE_SDK_ID = SHARE_SDK_ID;
//        sp.edit().putString("SHARE_SDK_ID", SHARE_SDK_ID).commit();
//    }
//
//    public String getWECHAT_APPID() {
//        if (TextUtils.isEmpty(WECHAT_APPID)) {
//            WECHAT_APPID = sp.getString("WECHAT_APPID", "");
//        }
//        return WECHAT_APPID;
//    }
//
//    public void setWECHAT_APPID(String WECHAT_APPID) {
//        this.WECHAT_APPID = WECHAT_APPID;
//        sp.edit().putString("WECHAT_APPID", WECHAT_APPID).commit();
//    }
//
//    public String getWECHAT_SECRET() {
//        if (TextUtils.isEmpty(WECHAT_SECRET)) {
//            WECHAT_SECRET = sp.getString("WECHAT_SECRET", "");
//        }
//        return WECHAT_SECRET;
//    }
//
//    public void setWECHAT_SECRET(String WECHAT_SECRET) {
//        this.WECHAT_SECRET = WECHAT_SECRET;
//        sp.edit().putString("WECHAT_SECRET", WECHAT_SECRET).commit();
//    }
//
//    public String getSINA_APPKEY() {
//        if (TextUtils.isEmpty(SINA_APPKEY)) {
//            SINA_APPKEY = sp.getString("SINA_APPKEY", "");
//        }
//        return SINA_APPKEY;
//    }
//
//    public void setSINA_APPKEY(String SINA_APPKEY) {
//        this.SINA_APPKEY = SINA_APPKEY;
//        sp.edit().putString("SINA_APPKEY", SINA_APPKEY);
//    }
//
//    public String getSINA_APPSECRET() {
//        if (TextUtils.isEmpty(SINA_APPSECRET)) {
//            SINA_APPSECRET = sp.getString("SINA_APPSECRET", "");
//        }
//        return SINA_APPSECRET;
//    }
//
//    public void setSINA_APPSECRET(String SINA_APPSECRET) {
//        this.SINA_APPSECRET = SINA_APPSECRET;
//        sp.edit().putString("SINA_APPSECRET", SINA_APPSECRET).commit();
//    }
//
//    public String getSINA_REDIRECT() {
//        if (TextUtils.isEmpty(SINA_REDIRECT)) {
//            SINA_REDIRECT = sp.getString("SINA_REDIRECT", "");
//        }
//        return SINA_REDIRECT;
//    }
//
//    public void setSINA_REDIRECT(String SINA_REDIRECT) {
//        this.SINA_REDIRECT = SINA_REDIRECT;
//        sp.edit().putString("SINA_REDIRECT", SINA_REDIRECT).commit();
//    }

}
