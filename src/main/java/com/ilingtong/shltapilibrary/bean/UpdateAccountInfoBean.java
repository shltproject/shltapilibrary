package com.ilingtong.shltapilibrary.bean;

/**
 * @ClassName: UpdateAccountInfoBean
 * @Package: com.ilingtong.shltapilibrary.bean
 * @Description: 修改用户信息实体类
 *               used by 1049
 * @author: liuting 修改用户信息
 * @Date: 2017/5/12 15:24
 */

public class UpdateAccountInfoBean {
    public String  user_id;        //用户ID
    public String  user_name;      //用户名称
    public String  user_nick_name; //用户昵称
    public String  user_phone;     //手机号
    public String  user_photo_url;  //头像URL
    public String  user_sex;       //性别
    public String  user_id_no;     //身份证号
    public String  user_sales_point;
    public String  user_rebate_point;
    public String  user_account_balance;
    public String  user_coupons;
    public String  user_fans;
    public String  user_follows;
    public String  user_qr_code_url;
    public String  user_customs_flag;
}
