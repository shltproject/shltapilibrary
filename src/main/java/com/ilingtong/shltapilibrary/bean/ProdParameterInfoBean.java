package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 商品规格参数
 * use by 1023获取商品规格参数
 */
public class ProdParameterInfoBean implements Serializable{
    //参数模块名称
    public String module_name;
    //参数列表
    public ArrayList<ProdParameterSublistinfoBean> parameters;
}
