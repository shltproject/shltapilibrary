package com.ilingtong.shltapilibrary.bean;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description: 购物车中商品列表属性。
 * 1027接口删除购物车入口参数detail的实例
 * 1029接口新增订单入口参数product_list的泛型
 */
public class ShopCartProdRequstBean {
    //明细序号
    public String seq_no;
    //商品ID
    public String product_id;

    public ShopCartProdRequstBean() {
    }

    public ShopCartProdRequstBean(String seq_no, String product_id) {
        this.seq_no = seq_no;
        this.product_id = product_id;
    }
}
