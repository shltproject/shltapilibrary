package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description: 用户护照，入境等附属信息对象
 * use by 11006 最外层结构
 */
public class UserEntryBean implements Serializable {
    public UserEntryInfoBean immigration_info;
}
