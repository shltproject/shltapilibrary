package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description:商品规格对象。为商品所有规格属性及规格属性下可供选择的规格列表项。区别于购物车的规格
 * use by 1022获取商品详情
 */
public class ProdSpecTypeInfoBean implements Serializable {
    public String prod_spec_id;
    public String prod_spec_name;
    public String can_be_modify_flag;
    public ArrayList<ProdSepcDetailBean> spec_detail_list;
}
