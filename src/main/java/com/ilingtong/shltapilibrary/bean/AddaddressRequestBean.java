package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/7
 * Time: 14:44
 * Email: jqleng@isoftstone.com
 * Desc: 添加新地址中需要的地址属性类
 * 1031接口入口参数add_info的对象
 */
public class AddaddressRequestBean implements Serializable{
    public String consignee;
    public String tel;
    public String province_id;
    public String city_id;
    public String area_id;
    public String address;
    public String post_code;

}
