package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Package:com.ilingtong.shltapilibrary.bean
 * author:liuting
 * Date:2016/11/3
 * Desc:优惠券列表信息
 * used by 6015(最外层结构)
 */

public class UsefulCouponBean implements Serializable {
    public ArrayList<UsefulCouponBaseInfoBean> voucher;  //我的可用优惠券列表
}
