package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/28.
 * mail: wuqian@ilingtong.com
 * Description: 立即下单 入口参数order_info实体
 * use by 12003（入口参数）
 */
public class OrderCreateNowOrderRequestBean implements Serializable {
    //订单信息（商品明细列表（明细序号，商品ID)
    public OrderBuyProdRequestBean product_info;
    //收货地址编号
    public String address_no = "";
    //支付方式编号
    public String pay_type = "";
    //是否需要发票
    public String invoice_type;
    //配送信息（配送方式编号，配送时间）
    public ShippingInfoReuqestBean shipping_method;
    //发票信息（发票类型编号，发票内容区分，发票抬头备注）
    public InvoiceInfoRequestBean invoice_info;
    //订单备注
    public String order_memo = "";
    //关税
    public String tariff = "";
    //优惠券号
    public String vouchers_number_id = "";

    public String post_id = "";
    public String wifi_id = "";
    public String relation_id = "";
    public String since_some_id = "";
    public String receive_date = "";
    public String receive_time = "";
    public String tax_flg = "";
}
