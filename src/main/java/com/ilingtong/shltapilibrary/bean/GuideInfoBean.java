package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * @ClassName:GuideInfoBean
 * @author:fengguowei
 * @date:2017/11/20
 * @Description: 导游信息实体类
 * used by 61008
 */

public class GuideInfoBean implements Serializable {
    public String guide_id;
    public String guide_name;
    public String guide_phone;
    public String guide_photo;
}
