package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Package:com.ilingtong.shltapilibrary.bean
 * author:liuting
 * Date:2016/11/3
 * Desc:优惠券信息类
 * used by 6015
 */

public class CouponBaseInfoBean implements Serializable {
    public String vouchers_number_id;  //抵用券券号
    public String vouchers_name;   //抵用券名称
    public String issue_flg;    //发行区分
    public String issue_store_name;   //发行店铺名
    public double money;   //金额
    public double use_conditions;   //使用条件
    public String use_conditions_memo;   //使用条件说明
    public String expiration_date_begin;    //有效期开始日
    public String expiration_date_end;    //有效期结束日
    public String vouchers_status;   //抵用券状态  1-未领取  2-已领取  3-已使用   4-已过期
}
