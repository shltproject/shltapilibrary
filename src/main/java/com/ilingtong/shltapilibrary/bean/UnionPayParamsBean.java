package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/12/12.
 * mail: wuqian@ilingtong.com
 * Description:中银支付地址
 * use by 11009
 */
public class UnionPayParamsBean implements Serializable{
    public String pay_url;   //支付页地址
}
