package com.ilingtong.shltapilibrary.bean;

/**
 * Created by wuqian on 2016/8/5.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class BaseResultBean<T> {
    public BaseHeaderBean head;
    public T body;

//    public T getBody() {
//        return body;
//    }
//
//    public void setBody(T body) {
//        this.body = body;
//    }
}
