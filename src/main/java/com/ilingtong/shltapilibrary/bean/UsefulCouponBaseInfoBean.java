package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Package:com.ilingtong.shltapilibrary.bean
 * author:liuting
 * Date:2016/11/3
 * Desc:我的可用优惠券信息
 * used by 6015
 */

public class UsefulCouponBaseInfoBean implements Serializable{
    public CouponBaseInfoBean voucher_base;  //我的可用优惠券

}
