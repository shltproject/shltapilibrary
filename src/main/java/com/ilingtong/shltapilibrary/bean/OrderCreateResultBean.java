package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/4/7.
 * mail: wuqian@ilingtong.com
 * Description:订单info类（新增订单）
 * use by 1029 新增订单
 */
public class OrderCreateResultBean implements Serializable {
    public OrderInfoCreateBean order_info;
}
