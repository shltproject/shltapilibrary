package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 帖子详情信息
 * use by 1089 帖子详情 json最外层
 */
public class PostUserPostBean implements Serializable {
    public PostUserPostInfoBean user_post_info;
}
