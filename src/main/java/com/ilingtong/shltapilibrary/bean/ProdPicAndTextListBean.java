package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 商品图文详情列表
 * use by 1024获取图文详情
 */
public class ProdPicAndTextListBean implements Serializable {
    public ArrayList<ProdPicAndTextInfoBean> prod_pic_text_list;
}
