package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description: 自提点详情
 * use by 11005获取自提点列表
 */
public class PickUpAddressInfoBean implements Serializable {
    public String since_some_id;             //	自提点id
    public String since_some_name;            //	自提点名称
    public String tel;            //	电话
    public String address;            //	详细地址
    public String memo;            //	备注
    public ArrayList<PickUpTimeBean> pick_up_time;            //	自提允许时间段
    public String group_date_from;   //开团开始日期   add on 2018/3/28
    public String group_date_to;     //开团结束日期   add on 2018/3/28

}
