package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description:商品规格参数列表
 * use by 1023 获取商品规格参数列表
 */
public class ProdParametersListBean implements Serializable {
    public ArrayList<ProdParameterInfoBean> prod_parameter_list;
}
