package com.ilingtong.shltapilibrary.bean;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description:规格明细对象。如：颜色：spec_detail_id：01；spec_detail_name：白色；spec_detail_id：02，spec_detail_name：红色。
 * use by 1022
 */
public class ProdSepcDetailBean {
    public String spec_detail_id;
    public String spec_detail_name;
}
