package com.ilingtong.shltapilibrary.bean;

import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description:基础数据列表详情类
 * use by 2018 sub_list为自嵌套list
 */
public class BaseDataInfoBean {
    public String code;
    public String name;
    public ArrayList<BaseDataInfoBean> sub_list;
}
