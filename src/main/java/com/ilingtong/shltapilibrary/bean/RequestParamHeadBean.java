package com.ilingtong.shltapilibrary.bean;


import com.ilingtong.shltapilibrary.ApiLibApplication;

/**
 * Created by wuqian on 2016/8/9.
 * mail: wuqian@ilingtong.com
 * Description: 请求接口时传入的入口参数head的实例
 */
public class RequestParamHeadBean {
    private String user_id;
    private String user_token;
    private String app_inner_no;
    private String language;

    public RequestParamHeadBean(String user_id, String user_token) {
        this.app_inner_no = ApiLibApplication.mInstance.getmConfigInfoBean().getApp_inner_no();
        this.language = ApiLibApplication.mInstance.getLanguage();
        this.user_id = user_id;
        this.user_token = user_token;
    }
}
