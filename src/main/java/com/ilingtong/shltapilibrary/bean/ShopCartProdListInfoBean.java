package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/10/20.
 * mail: wuqian@ilingtong.com
 * Description:购物车商品列表
 * use by 11008
 */
public class ShopCartProdListInfoBean implements Serializable {
    public String seq_no;              //	明细序号
    public String post_id;              //	帖子ID
    public String prod_pic_url;              //	商品缩略图URL
    public String prod_id;              //	商品编码
    public String prod_name;              //	商品名称
    public double  price;              //	价格
    public double  point;              //	积分
    public int  order_qty;              //	数量
    public double  amount;              //	金额
    public double  fee_amount;              //	运费
    public String relation_id;              //	二维码关联ID
    public ArrayList<ProdSpecListInfoBean> prod_spec_list;              //	规格列表
    public String country_pic_url;              //	进口国图片
    public String import_info_desc;              //	进口信息描述
    public String transfer_fee_desc;              //	配送费描述
    public String tariff_desc;              //	关税描述
    public double tariff;              //	实际关税
    public String import_goods_flag;              //	进口商品标识
    public double buy_limit_qty;              //	商品限购数量
    public String mstore_id;              //	魔店ID

}
