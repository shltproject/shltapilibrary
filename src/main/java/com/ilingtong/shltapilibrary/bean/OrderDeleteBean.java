package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Package:com.ilingtong.shltapilibrary.bean
 * author:liuting
 * Date:2016/10/25
 * Desc:取消支付退款信息对象
 * json最外层
 * use by 11007
 */

public class OrderDeleteBean implements Serializable {
    public String  msg_info;//取消已付款订单并退款返回信息
}
