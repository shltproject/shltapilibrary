package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/26.
 * mail: wuqian@ilingtong.com
 * Description: 扫描到的二维码信息结果解析详情
 *              扫描到的二维码可能是 商品/帖子/会员/魔店/组织
 * use by 1064
 */
public class ScanQrDatadecodeBean implements Serializable {
    public String code_type;
    public String prod_id;
    public String post_id;
    public String user_id;
    public String store_id;
    public String relation_id;
    public String org_id;   //组织id
}
