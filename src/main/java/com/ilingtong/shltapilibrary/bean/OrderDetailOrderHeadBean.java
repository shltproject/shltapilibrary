package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 订单详情 订单头信息
 * use by 1041 获取订单详情接口
 *
 */
public class OrderDetailOrderHeadBean implements Serializable {
    public String order_no;
    public String order_time;
    public String order_memo;
    public String quantity;
    public String order_total_amount;  //订单金额
    public String fee_amount;
    public String status;
    public String pay_method;
    public String modify_flag;
    public String cancle_flag;
    public String evaluate_flag;
    public String tariff;
    public String customs_flag;
    public String customs_fail_reason;
    public String goods_amount;    //add at 2016/04/07  商品总额
    public String pay_amount;    //add at 2016/04/07    订单金额减去优惠券抵扣后的实付金额
}
