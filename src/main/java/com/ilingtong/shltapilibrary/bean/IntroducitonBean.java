package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description:功能介绍页的标题和h5地址
 * use by 2026
 */
public class IntroducitonBean implements Serializable{
    public String title_text;
    public String function_introduction_url;
}
