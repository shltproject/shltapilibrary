package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/18.
 * mail: wuqian@ilingtong.com
 * Description:1025加入购物车接口入口参数product_info的对象属性
 * use by 1025
 */
public class ProdCartItemRequestBean implements Serializable {
    public String product_id;
    public int order_qty;
    public ArrayList<ProdSpecListInfoBean> prod_spec_list;
}
