package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description:自提点列表
 * use by 11005 获取自提点列表
 */
public class PickUpAddressListBean implements Serializable {
  public ArrayList<PickUpAddressInfoBean> since_some_list;
}
