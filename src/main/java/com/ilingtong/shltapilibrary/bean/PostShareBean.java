package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Package:com.ilingtong.shltapilibrary.bean
 * author:liuting
 * Date:2016/10/31
 * Desc:帖子转发信息类，该类包含通过转发返回的帖子信息
 * used by 1020
 */

public class PostShareBean implements Serializable {
    public String my_repost_id;//转发后新帖子id
    public String post_link_url;//转发后帖子链接url
    public String post_image_url;//转发后帖子缩略图url
    public String magazine_title;//转发后帖子标题
    public String wx_profile;//转发后微信转帖说明
    public String magazine_describle;//转发后帖子的转发内容
}
