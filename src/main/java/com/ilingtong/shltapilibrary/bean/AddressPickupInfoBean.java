package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description: 自提点地址
 * use by 1040获取我的订单列表
 * use by 1041
 */
public class AddressPickupInfoBean implements Serializable {
    public String since_some_id;   //自提点Id
    public String since_some_name;  //自提点名称
    public String address;  //详细地址
    public String memo;  //备注
}
