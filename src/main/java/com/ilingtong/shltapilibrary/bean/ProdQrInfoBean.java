package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/23.
 * mail: wuqian@ilingtong.com
 * Description: 商品二维码信息
 * use by 1085获取商品二维码
 */
public class ProdQrInfoBean implements Serializable{
    public String prod_qr_code_url;
    public String latest_relation_id;  //新二维码关联id
}
