package com.ilingtong.shltapilibrary.bean;

/**
 * @ClassName: UpdateAccountInfoRequestBean
 * @Package: com.ilingtong.shltapilibrary.bean
 * @Description: 修改用户信息接口入口参数  used by 1049
 * @author: liuting
 * @Date: 2017/5/12 16:28
 */

public class UpdateAccountInfoRequestBean {
    public String head_photo_binary_data = "";//头像字符串
    public String user_nick_name = "";//昵称
    public String sex = "";//性别
    public String id_no = "";//身份证号
    public String customs_flag = "";//积分开关
    public String user_name = "";//真是姓名

}
