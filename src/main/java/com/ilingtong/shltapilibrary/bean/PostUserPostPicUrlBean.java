package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description:帖子详情post_thumbnail_pic_url中的pic_url
 * use by 1089
 */
public class PostUserPostPicUrlBean implements Serializable {
    public String pic_url;
}
