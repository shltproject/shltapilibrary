package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 商品图文详情信息
 * use by 1024获取商品图文详情
 */
public class ProdPicAndTextInfoBean implements Serializable {
    public String pic_url;  //图片地址
    public String pic_memo;  //图片文字描述
}
