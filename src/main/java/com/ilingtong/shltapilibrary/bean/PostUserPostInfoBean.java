package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 个人帖子详细信息对象。包含转帖信息和原帖信息
 * use by 1089 帖子详情
 */
public class PostUserPostInfoBean implements Serializable {
    public String user_id;
    public String user_nick_name;
    public String user_signature;
    public String user_favorite_by_me;
    public String post_comment;
    public String user_head_photo_url;
    public String post_id;
    public String post_title;
    public String post_time;
    public String post_favorited_by_me;
    public String cover_picture_url;
    public ArrayList<PostUserPostContentBean> post_content;  //帖子内容列表
    public ArrayList<PostUserPostPicUrlBean> post_thumbnail_pic_url;  //帖子缩略图列表
    public ArrayList<PostUserPostPicUrlBean> first_post_thumbnail_pic_url;
    public String first_user_id;
    public String first_user_nick_name;
    public String first_user_signature;
    public String first_user_head_photo_url;
    public String first_post_id;
    public String first_post_title;
    public String first_post_time;
}
