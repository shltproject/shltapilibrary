package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 商品列表的列表详情内容
 * use by 2019 获取我收藏的商品列表
 */
public class ProdListItemBean implements Serializable{
   public  String prod_id;
   public  String prod_name;
   public  String prod_thumbnail_pic_url;
   public  double prod_price;
   public  int prod_point;
   public  String prod_points_rule;
   public  String post_id;
   public  String mstore_id;
   public  String prod_favorited_by_me;
   public  String trade_prod_favorited_by_me;
   public  String relation_id;
   public  String conpon_flag;
   public  String goods_type;
}
