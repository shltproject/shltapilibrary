package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description:订单详情类
 * use by 1041 获取订单详情信息 json最外层
 */
public class OrderDetailOrderBean implements Serializable {
    public OrderDetailOrderInfoBean order_info;
}
