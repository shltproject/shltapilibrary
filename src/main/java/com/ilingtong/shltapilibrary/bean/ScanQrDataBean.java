package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/26.
 * mail: wuqian@ilingtong.com
 * Description:二维码解析 信息
 * use by 1064 二维码扫描结果解析
 */
public class ScanQrDataBean implements Serializable {
    public ScanQrDatadecodeBean qr_data;
}
