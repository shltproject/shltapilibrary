package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description: 自提时间段
 * use by 11005
 */
public class PickUpTimeBean implements Serializable {
    public String receive_time;  //自提时间段
}
