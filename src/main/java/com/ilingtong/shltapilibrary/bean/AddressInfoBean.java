package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * User: wuqian
 * Date: 16/9/12
 * Time: 22:10
 * Email:
 * Desc: 地址属性类，地址列表中的各项均为该属性，因为要通过参数传递给其他activity,所以设置为Serializable
 * use by 1030 接口
 * use by 2023 设置默认收货地址
 * use by 1033 删除收货地址
 */
public class AddressInfoBean implements Serializable{
    public String address_no;
    public String user_tag;
    public String consignee;
    public String tel;
    public String province_id;
    public String province_name;
    public String area_id;
    public String city_id;
    public String city_name;
    public String area_name;
    public String address;
    public String post_code;
    public String default_flag;

    @Override
    public String toString() {
        return "address_no:" + address_no + "\r\n" +
                "user_tag:" + user_tag + "\r\n" +
                "consignee:" + consignee + "\r\n" +
                "tel:" + tel + "\r\n" +
                "province_id:" + province_id + "\r\n" +
                "province_name:" + province_name + "\r\n" +
                "area_id:" + area_id + "\r\n" +
                "city_name:" + city_name + "\r\n" +
                "area_name:" + area_name + "\r\n" +
                "address:" + address + "\r\n" +
                "post_code:" + post_code + "\r\n" +
                "default_flag:" + default_flag;
    }

}
