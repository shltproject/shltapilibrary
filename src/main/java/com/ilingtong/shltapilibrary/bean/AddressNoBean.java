package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description: 地址编号对象。
 * 用于删除收货地址入口参数对象化
 */
public class AddressNoBean implements Serializable{
    public String address_no;

    public AddressNoBean(String address_no) {
        this.address_no = address_no;
    }
}
