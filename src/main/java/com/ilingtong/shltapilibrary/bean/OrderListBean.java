package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description:订单列表信息类
 * 共通块9007
 * use by 1040接口
 */
public class OrderListBean implements Serializable {
    public int data_total_count;   //总数量
    public ArrayList<OrderInfoBean> order_list;  //订单列表
}
