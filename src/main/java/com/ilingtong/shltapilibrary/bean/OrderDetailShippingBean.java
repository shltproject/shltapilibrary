package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 配送方式信息
 * use by 1041
 */
public class OrderDetailShippingBean implements Serializable {
    public String shipping_method_no;
    public String shipping_method_name;
    public String shipping_time_memo;
}
