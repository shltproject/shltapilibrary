package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/26.
 * mail: wuqian@ilingtong.com
 * Description:商品结算结果信息
 * use by 2042
 * use by 12002
 */
public class SettlementInfoBean implements Serializable {
    public double amount_payable;    //应付金额
    public double total_fee;
    public double total_tariff;
    public int total_goods_count;
    public double total_goods_price;
    public String vouchers_number_id;       //  优惠券号
    public String vouchers_name;      //   券名
    public double money;      //    优惠金额
    public String use_conditions;      //    使用条件
    public double order_total_amount;      //   订单金额
    public ArrayList<ShopCartListInfoBean> my_shopping_cart;  //注意：接口返回字段跟ShopCartListInfoBean不完全一致，如果有修改。如果有修改注意审查
    public String issue_flg;
    public String transport_way;
    public int total_points;
    public int available_point;
}
