package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 商品评价信息
 * use by 1046 获取商品评价
 */
public class ProdEvaluationInfoBean implements Serializable {
    public String rating_doc_no; //评价单号
    public float level;  //评价等级
    public String rating_date; //评价日期
    public String user_nick_name; //会员昵称
    public String head_photo_url; //会员头像
    public String memo; //评价内容
}
