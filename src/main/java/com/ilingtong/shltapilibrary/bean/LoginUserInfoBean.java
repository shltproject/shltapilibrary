package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/8/8.
 * mail: wuqian@ilingtong.com
 * Description:userinfo
 * use by 11002登录
 */
public class LoginUserInfoBean implements Serializable {
    public String user_id;                   //用户ID
    public String user_type;                //用户区分
    public String is_first_login;           //是否首次登录
    public String require_change_pwd;       //是否必须修改密码
    public String user_nick_name;           //用户昵称
    public String user_head_photo_url;      //用户头像URL
    public String user_sex;                 //用户性别
    public String token;                    //Token
    public String is_protocol_show;         //酬客协议显示标志
    public String payoff_protocol_url;      //酬客协议URL
    public String userPhone;                //用户手机
    public String external_id;              //外部id
    public String is_edit;                  //是否弹出个人信息窗口
    public String is_visa_expired;          //是否签证过期
    public String home_url;                 //首页URL
    public String cate_url;                 //分类URL
    public String upd_md5;                  //用户出入境信息更新标识


}
