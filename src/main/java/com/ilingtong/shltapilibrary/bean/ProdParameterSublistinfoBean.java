package com.ilingtong.shltapilibrary.bean;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 商品规格参数子项列表信息
 * use by 1023 获取商品规格信息
 */
public class ProdParameterSublistinfoBean {
    //规格名称
    public String item_name;
    //规格值
    public String item_value;
}
