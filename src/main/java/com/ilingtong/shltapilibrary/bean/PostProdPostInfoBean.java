package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description:商品详情中的帖子信息
 * use by 1022获取商品信息
 */
public class PostProdPostInfoBean implements Serializable {
    public String post_id;
    public String my_post_flag;
    public String post_comment;
    public String prod_link_url;
}
