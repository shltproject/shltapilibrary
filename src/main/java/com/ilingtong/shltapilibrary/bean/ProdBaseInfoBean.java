package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/14.
 * mail: wuqian@ilingtong.com
 * Description:商品详情基本信息(包含商品详情和帖子信息)实体
 * use by 1022 获取商品详情
 */
public class ProdBaseInfoBean implements Serializable {
    public ProdInfoBean prod_info;
    public PostProdPostInfoBean post_info;
}
