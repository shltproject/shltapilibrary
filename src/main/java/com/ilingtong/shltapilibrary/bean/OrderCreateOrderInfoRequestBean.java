package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/27.
 * mail: wuqian@ilingtong.com
 * Description:订单下单信息 新增订单接口入口参数order_info实体
 * use by 1029 （入口参数）
 * 退税版 配送信息区分：配送方式编号永远不能为空。当配送方式为快递时 配送时间不能为空，自提点id和提货日期，提货时间段为空
 * 反之 配送时间为空，自提点和提货日期，提货时间段不能为空
 * 是否需要发票为否，发票信息为空。
 */
public class OrderCreateOrderInfoRequestBean implements Serializable {
    //订单信息（商品明细列表（明细序号，商品ID)
    public ArrayList<ShopCartProdRequstBean> product_list;
    //收货地址编号
    public String address_no;
    //支付方式编号
    public String pay_type;
    //是否需要发票
    public String invoice_type;
    //配送信息（配送方式编号，配送时间）
    public ShippingInfoReuqestBean shipping_method;
    //发票信息（发票类型编号，发票内容区分，发票抬头备注）
    public InvoiceInfoRequestBean invoice_info;
    //订单备注
    public String order_memo = "";
    //关税
    public String tariff = "";
    //优惠券号 add  at 2016/04/05
    public String vouchers_number_id = "";
    public String since_some_id = "";   //自提点id
    public String receive_date = "";  //提货日期
    public String receive_time = "";   //提货时间段
    public String tax_flg = "";  //退税区分
}
