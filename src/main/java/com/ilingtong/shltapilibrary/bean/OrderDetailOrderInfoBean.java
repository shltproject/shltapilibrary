package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 订单详情 订单信息类
 * use by 1041获取订单详情
 */
public class OrderDetailOrderInfoBean implements Serializable {
    public OrderDetailOrderHeadBean head_info;
    public ArrayList<OrderDetailOrderDetailBean> order_detail;
    public OrderDetailAddressBean add_info;
    public String pay_type_id;
    public String pay_type_name;
    public OrderDetailShippingBean shipping_info;
    public OrderDetailInvoiceBean invoice_info;
    public OrderDetailLogisticsBean logistics_info;
    public AddressPickupInfoBean since_some_info;  //待收货地址
    public String goods_return_url;
    public String goods_return_title;
    public String vouchers_nam;      // add at 2016/04/07  抵用券名
    public String money;      // add at 2016/04/07   抵用券金额
    public String use_conditions;      // add at 2016/04/07  抵用券使用条件
    public String receive_date;  //自提时间 日期
    public String receive_time;  //自提时间 时间段
}
