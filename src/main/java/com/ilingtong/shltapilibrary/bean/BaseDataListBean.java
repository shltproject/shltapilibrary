package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/13.
 * mail: wuqian@ilingtong.com
 * Description:基础数据列表
 * use by 2018接口 最外层实体
 */
public class BaseDataListBean implements Serializable {
    public ArrayList<BaseDataListInfoBean> base_data_list;
}
