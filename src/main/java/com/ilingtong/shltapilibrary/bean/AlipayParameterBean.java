package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/30.
 * mail: wuqian@ilingtong.com
 * Description: 支付宝支付的支付参数
 * use by 2034
 */
public class AlipayParameterBean implements Serializable {
    public String alipay_info;
}
