package com.ilingtong.shltapilibrary.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wuqian on 2016/9/22.
 * mail: wuqian@ilingtong.com
 * Description: 商品列表
 * 共通块 9003
 * use by 2019获取收藏的商品列表
 */
public class ProdListBean implements Serializable{
    public int data_total_count;
    public ArrayList<ProdListItemBean> prod_list;//我收藏的商品列表
}
